/*
 * Arvores binarias de busca.
 */


#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "abb.h"

void cria_arvore(Arvore *p){
	*p = NULL;
}

void inorder(Arvore p) {
	if (p != NULL) {
		inorder (p->esq);
		printf("%d ", p->info);    
		inorder (p->dir);    
	}
}

/* Retorna 1 se a chave for encontrada */
int busca (Arvore p, int chave) {
	if(p == NULL){ // caso base
		return 0;
	}
	if (chave < p->info)
		return busca(p->esq, chave);
	else if (chave > p->info)
		return busca(p->dir, chave);
	return 1;
}

/* Retorna 1 se a chave for encontrada */
// versao nao recursiva
int n_rec_busca(Arvore p, int chave) {
	while(p!= NULL){
		if (chave < p->info)
			p = p->esq;
		else if (chave > p->info)
			p = p->dir;
		else
			return 1;	
	}
	return 0;
}

/* Retorna 0 se a chave for repetida */
int  insere(Arvore *p, int chave) {
	No *novo;
	
	if(*p == NULL){ // caso base
		novo = malloc (sizeof(No));
		novo->info = chave;
		novo->esq = NULL;
		novo->dir = NULL;
		*p = novo;
		return 1;
	}
	if (chave < (*p)->info)
		return insere(&((*p)->esq), chave);
	else if (chave > (*p)->info)
		return insere(&(*p)->dir, chave);
	return 0;
}

/* Retorna 0 se a chave for repetida */
/* Vers�o n�o recursiva */
int  n_rec_insere(Arvore *p, int chave) {
	No *novo;
	
	while(*p != NULL){
		if (chave < (*p)->info)
			p = &(*p)->esq;
		else if (chave > (*p)->info)
			p = &(*p)->dir;
		else
			return 0;
	}
	novo = malloc (sizeof(No));
	novo->info = chave;
	novo->esq = NULL;
	novo->dir = NULL;
	*p = novo;	
	return 1;
}

/* Retorna 0 se a chave nao for encontrada */
int remove_arv(Arvore *p, int chave) {
	No *rem, *sub;
	No **p_aux;
	while(*p != NULL && chave != (*p)->info){
		if (chave < (*p)->info)
			p = &(*p)->esq;
		else 
			p = &(*p)->dir;
	}
	
	if (*p == NULL)
		return 0;
		
	// eh folha
	if ((*p)->esq == NULL && (*p)->dir == NULL){
		free(*p);
		*p = NULL;
		return 1;
	}
	
	// tem um unico filho
	if ((*p)->esq == NULL){
		rem = *p;
		*p = (*p)->dir;
		free(rem);
		return 1;
	}
	if ((*p)->dir == NULL){
		rem = *p;
		*p = (*p)->esq;
		free(rem);
		return 1;
	}
		
	// tem dois filhos
	rem = *p;
	p_aux = &(*p)->dir;
	while((*p_aux)->esq != NULL)
		p_aux = &(*p_aux)->esq;

	sub = *p_aux;

	if (sub != rem->dir){
		*p_aux = sub->dir;
		sub->dir = rem->dir;
	}
	sub->esq = rem->esq;
	*p = sub;
	free (rem);
	return 1;
	
}

/* Verifica se p e uma arvore de busca */
int verifica_busca(Arvore p)  {
	if (p == NULL)
		return 1;
	if (verifica_busca(p->esq) && verifica_busca(p->dir)){
		if ((p->esq != NULL && verifica_maior(p->esq) > p->info) || (p->dir != NULL && verifica_menor(p->dir) < p->info))
			return 0;
		return 1;
	}
	return 0;
}

int verifica_menor(Arvore p){
	while(p->esq != NULL)
		p = p->esq;
	return p->info;
}

int verifica_maior(Arvore p){
	while(p->dir != NULL)
		p = p->dir;
	return p->info;
}

// http://www.geeksforgeeks.org/a-program-to-check-if-a-binary-tree-is-bst-or-not/

int verifica_busca2(Arvore p) {
  return eh_ABB(p, INT_MIN, INT_MAX);
}

int eh_ABB(Arvore p, int min, int max){
  if (p == NULL)
    return 1;

  if (p->info < min || p->info > max)
    return 0;

  return (eh_ABB(p->esq, min, p->info - 1) && 
	  eh_ABB(p->dir, p->info + 1, max));
}

