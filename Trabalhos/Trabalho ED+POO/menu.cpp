/*
menu.cpp
Desenvolvedores:
	Dominik Reller							587516
	Elisa Marques de Castro					587303
	Leticia Mara Berto						587354
	Luis Augusto Fran�a Barbosa				511374

Este programa desenvolve um corretor de texto, baseando-se nas palavras de um
dicionario. No caso de palavras erradas fornece op��es de melhoria no texto.

Ultima modifica��o: 10/06/2015
*/

#include <iostream>
#include <string>
#include "corretor.h"

using namespace std;

int main() {
	
	cout << "Digite o nome do arquivo texto: " << endl;
	string nomeArq; // Recebe nome do arquivo que contem o texto
	cin >> nomeArq;
	
	// Inicializa Corretor, passando como parametro o nome do arquivo
	Corretor corretor(nomeArq);
	
	// Carrega dicionario
	//Dicionario dicionario;
	//dicionario.carregarDic();

	// Modulo prinipal para verificar erros do texto
	corretor.verificaErro();

	// Gravar texto
	//Texto texto("teste.txt");
	//texto.gravarTexto("saida.txt");
	
	// Gravar dicionario

	return 0;
}
