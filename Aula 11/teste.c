#include <stdio.h>
#include <stdlib.h>
#include "avl.h"

int main(){
  Arvore a;
  int aux;

  //a = arv(14, NULL, arv(16, NULL, NULL));
  a = arv(14, NULL, arv(16, NULL, arv(18, NULL, NULL)));
  inorder(a);
  RR(&a);

  insere(&a, 2, &aux);
  printf("\n");
  if (verifica_AVL(a))
    printf("Raiz %d\n", a->v);
  inorder(a);

  insere(&a, 1, &aux);
  printf("\n");
  if (verifica_AVL(a))
    printf("Raiz %d\n", a->v);
  inorder(a);

  insere(&a, 10, &aux);
  printf("\n");
  if (verifica_AVL(a))
    printf("Raiz %d\n", a->v);
  inorder(a);

  insere(&a, 5, &aux);
  printf("\n");
  if (verifica_AVL(a))
    printf("Raiz %d\n", a->v);
  inorder(a);

  insere(&a, 11, &aux);
  printf("\n");
  if (verifica_AVL(a))
    printf("Raiz %d\n", a->v);
  inorder(a);

  insere(&a, 7, &aux);
  printf("\n");
  if (verifica_AVL(a))
    printf("Raiz %d\n", a->v);
  inorder(a);

  insere(&a, 12, &aux);
  printf("\n");
  if (verifica_AVL(a))
    printf("Raiz %d\n", a->v);
  inorder(a);

  insere(&a, 20, &aux);
  printf("\n");
  if (verifica_AVL(a))
    printf("Raiz %d\n", a->v);
  inorder(a);

  libera(&a);
  return 0;
}
