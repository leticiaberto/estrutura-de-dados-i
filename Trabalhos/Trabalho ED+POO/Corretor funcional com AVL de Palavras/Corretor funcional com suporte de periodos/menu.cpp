/*
menu.cpp
Desenvolvedores:
	Dominik Reller							587516
	Elisa Marques de Castro					587303
	Leticia Mara Berto						587354
	Luis Augusto Fran�a Barbosa				511374

Este programa desenvolve um corretor de texto, baseando-se nas palavras de um
dicionario. No caso de palavras erradas fornece op��es de melhoria no texto.

Ultima modifica��o: 10/06/2015
*/

#include <iostream>
#include <string>
#include "corretor.h"

using namespace std;

int main() {
	
	cout << "Digite o nome do arquivo texto: " << endl;
	string nomeArq; // Recebe nome do arquivo que contem o texto
	cin >> nomeArq;
	
	// Inicializa Corretor, passando como parametro o nome do arquivo que contem o conteudo de texto
	// Dicionario � carregado quando o corretor chama o contrutor de dicionario
	Corretor corretor(nomeArq);

	// Modulo principal para verificar erros do texto
	// Ao final da correcao de erros pergunta para o usuario se quer ver a lista de erros.
	// Acabado de ver a lista de erros pergunta o nome do arquivo que sera salvado o texto e finalmente grava o dicionario com suas novas palavras(caso tenha sido inserido alguma) em dic.txt
	corretor.verificaErro();

	return 0;
}
