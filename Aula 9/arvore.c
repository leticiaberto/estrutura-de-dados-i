#include <stdio.h>
#include <stdlib.h>
#include "arvore.h"

No *arv(elem_t c, No* esq, No* dir){
	No *novo;
	novo = malloc (sizeof(No));
	novo->info = c;
	novo->esq = esq;
	novo->dir = dir;
	return novo;
}

void preorder(No* p){
	if (p != NULL) {
		printf("%c ", p->info);
		preorder(p->esq);
		preorder(p->dir);
	}
}

void inorder(No* p){
	if (p != NULL) {
		inorder(p->esq);
		printf("%c ", p->info);
		inorder(p->dir);
	}
}

void postorder(No* p){
	if (p != NULL) {
		postorder(p->esq);
		postorder(p->dir);
		printf("%c ", p->info);
	}
}

void largura(No* p){
	No* fila[100], *aux;
	int ini, fim;
	
	ini = fim = 0;
	if (p != NULL){
		fila[fim++] = p;
		// enquanto fila nao esta vazia
		while(ini != fim){
			aux = fila[ini++];
			printf("%c ", aux->info);
			if (aux->esq != NULL)
				fila[fim++] = aux->esq;			
			if (aux->dir != NULL)
				fila[fim++] = aux->dir;
		}		
	}
}

void libera(Arvore* p){
	No *aux = *p;
	if (*p != NULL) {
		if (aux->dir == NULL && aux->esq == NULL){
			free(aux);
		}
		else {
			libera(&aux->esq);
			libera(&aux->dir);
			free(aux);
		}
		*p = NULL;
	}
}

No* copia(No* p){
	No *novo;
	if (p != NULL) {
		novo = malloc (sizeof (No));
		novo->esq = copia (p->esq);
		novo->dir = copia (p->dir);
		novo->info = p->info;
		return novo;
	}
	
	return p;
}

int altura (No* p){
	int hesq, hdir;
	if (p == NULL)
		return 0;
	if (p->dir == NULL && p->esq == NULL)
		return 1;
	hesq = altura (p->esq);
	hdir = altura (p->dir);
	if (hesq > hdir)
		return hesq + 1;
	return hdir + 1;
}

int nNos (No* p){
	int nNosEsq, nNosDir;
	if (p == NULL)
		return 0;
	if (p->dir == NULL && p->esq == NULL)
		return 1;
	nNosEsq = nNos (p->esq);
	nNosDir = nNos (p->dir);
	return nNosEsq + nNosDir + 1;
}

int iguais(No* arv1, No* arv2){
	int esq, dir;
	if (arv1 == NULL && arv2 == NULL)
		return 1;
	if (arv1 == NULL || arv2 == NULL)
		return 0;
	if (arv1->info != arv2->info)
		return 0;
	esq = iguais (arv1->esq, arv2->esq);
	dir = iguais (arv1->dir, arv2->dir);
	return esq && dir;
}
		




