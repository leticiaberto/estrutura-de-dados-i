#include <stdio.h>
#include <stdlib.h>

#define MAX 10000

typedef struct no{
  char info;
  struct no *prox;
} Pilha;

void inicPilha(Pilha *p_l){
	p_l->prox = NULL;
}

//verifica o elemento do topo
char elemTopo(Pilha *p_l){

	if (p_l->prox == NULL)
		return '\0';
	return p_l->prox->info;
}

//empilhar
void push(Pilha *p_l, char e){
	Pilha *novo;
	novo = malloc (sizeof(Pilha));
	novo->info = e;
	novo->prox = p_l->prox;
	p_l->prox = novo;	
}

char pop(Pilha *p_l){
	Pilha *aux;
	char e = '\0';
	aux = p_l->prox;
	if (aux == NULL)
		return e;
	p_l->prox = aux->prox; // p_l->prox->prox
	e = aux->info;
	free (aux);
	return e;
}

/* Verifica se a Pilha est� vazia ou nao */
int PilhaVazia(Pilha *p_l){
	if (p_l->prox == NULL)
		return 1;
	return 0;
}


//ler a string
int read_line(char c[])
{
	int ch, i = 0;

	while ((ch = getchar()) != '\n')
	{
			c[i] = ch;
			i++;
	}
	c[i] = '\0';
	return i;
}
int main()
{
	char c[MAX];
	Pilha p;
	inicPilha(&p);
	int tam;
	tam = read_line(c);
	
	while (tam > 1) {
		
		int i = 0;
		while(c[i] != '\0')
		{	
			if (isalpha(c[i]))
				printf("%c",c[i]);
			else
			{
				while (elemTopo(&p) >= c[i])
					printf("%c", pop(&p));
				push(&p,c[i]);	
			}
			i++;
		}
		while(!PilhaVazia(&p))
		{
			printf("%c", pop(&p));
		}
	
		printf("\n");
		
		tam = read_line(c);
	}
	return 0;
} 
