/*
 * Programa para testar as opera��es de lista
 */

#include<stdio.h>
#include "lista_dinamica.h"

int main(){
  int num,num2, opcao,mod,div;
  Lista l,l2,l3;

  inicLista(&l);
  inicLista(&l2);  
  inicLista(&l3);

	printf("Entre com o primeiro numero: ");
	scanf("%d", &num);
	while(num > 0)
	{
		mod = num % 10000;
		num = num / 10000;
		//printf("Num: %d\n",num);
	 	insereFim(&l, mod);
	}
	///exibe(&l);
	printf("\nEntre com o segundo numero: ");
    scanf("%d", &num2);
	while(num2 > 0)
	{
		mod = num2 % 10000;
		num2 = num2 / 10000;
		//printf("Num2: %d\n",num2);
	  	insereFim(&l2, mod);
	}
	soma(&l,&l2,&l3);
	//exibe(&l2);
     
    printf("\n");
    printf("Lista 1: \n");
    exibe(&l);
    printf("\n\n");  
   
    printf("Lista 2: \n");  
	exibe(&l2);
	printf("\n\n");    
	
	printf("Soma das duas Listas: \n");
	exibe(&l3);
 	printf("\n\n"); 
      
  libera(&l);
  libera(&l2);
  libera(&l3);
  return 0;
}
