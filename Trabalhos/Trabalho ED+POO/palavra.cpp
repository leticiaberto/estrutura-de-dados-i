/*
palavra.cpp
Este m�dulo desenvolve a classe palavra, utilizado para armazenar cada palavra
do texto e tamb�m fornecer op��es para uma poss�vel corre��o da palavra se desejada.

Dados de entrada: Palavra do arquivo de texto
Dados de saida: Palavras para corre��o

Ultima modifica��o: 10/06/2015
*/

#include <cstring>
#include "palavra.h"

Palavra::Palavra() {

	palavra = "\0";
	
}

int Palavra::operator==( Palavra &palavra2 ) {
	if( palavra == palavra2.palavra )
		return 1;
	else
		return 0;
}

// Retorna string da palavra
string Palavra::getPalavra() {
	return palavra;
}

// Atualiza palavra
void Palavra::setPalavra( string word ) {
	palavra = word;
	//cout << word << endl;
}

// Retorna 1 caso seja semelhante, ou seja, caso tenha as duas primeiras letras iguais. 0 caso contrario
int Palavra::verificarSemelhante( string palavra_dic ) {
	// Strncmp retorna 0 caso as duas primeiras letras sejam iguais
	if( strncmp( palavra.c_str(), palavra_dic.c_str(), 2 ) )
		return 0;
	else
		return 1;
}
