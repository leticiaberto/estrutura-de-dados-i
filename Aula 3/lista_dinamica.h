/*
 * Implementacao de lista circular.
 */

#ifndef LISTAD_H
#define LISTAD_H

typedef int elem_t;

typedef struct no{
  elem_t info;
  struct no *prox;
} No_lista;

typedef No_lista Lista;

/* Inicializa uma lista */
void inicLista(Lista *p_l);

/* Verifica se a lista est� vazia ou nao */
int listaVazia(Lista *p_l);

/* Insere um elemento no inicio da lista */
void insereInicio(Lista *p_l, elem_t e);

/* Insere um elemento no final da lista */
void insereFim(Lista *p_l, elem_t e);

void soma(Lista *p_l, Lista *p_l2, Lista *p_l3);

/* Remove todos os numeros da lista */
void libera(Lista *p_l);

/* Exibe o conteudo da lista */
void exibe(Lista *p_l);

#endif
