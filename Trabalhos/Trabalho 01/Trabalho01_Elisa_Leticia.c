#include<stdio.h>
#include<stdlib.h>

int flag = 0;

#define TAM_VET_CAB 10
#define TAM_MAX_LISTA 10
#define TAM_NOME 30
#define TAM_FLAG 100

#define INSERIDO "Inserido com sucesso!\n"
#define CODIGO_USADO "O código já está sendo usado!\n"
#define LISTAS_CHEIAS "Listas Cheias!\n"
#define LISTAS_APAGADAS "Todas as listas foram apagadas!\n"
#define PACIENTE_N_ENCONTRADO "Paciente não encontrado!\n"
#define PACIENTE_REMOVIDO "%s foi removido!\n"

typedef struct pac {
    int cod;
    char nome[TAM_NOME];
} Paciente;

typedef struct no {
    Paciente info;
    struct no *prox;
} No_lista;

typedef struct cab {
    int tam;
    struct no *primNo;
    struct no *ultimoNo;
} No_cabeca;

typedef No_cabeca Lista;

void inic_vet(Lista vet_cab[]) {
    int i;

    for (i = 0; i < TAM_MAX_LISTA; i++) {
        vet_cab[i].primNo = NULL;
        vet_cab[i].ultimoNo = NULL;
        vet_cab[i].tam = 0;
    }
}

//retorna 1 se está cheio, 0 se está vazio
int cheio(Lista *p_l) {
	if (p_l->tam == TAM_MAX_LISTA) {
		return 1;
	} else {
		return 0;
	}
}

int auxInsere(Lista *p_l, Paciente p) {
	int i, j;
	No_lista *aux2;
	i = 0;

	if(flag < TAM_FLAG)
	{
		for(i = 0; i < TAM_VET_CAB; i++) {
			if(p_l[i].primNo == NULL)
			{
				inserePaciente(&p_l[i], p);
				flag++;
				return 1;
			}
			else
			{
				if(p_l[i].tam < TAM_MAX_LISTA)//tem espaço na lista, só insere
				{
					inserePaciente(&p_l[i], p);
					flag++;
					return 1;
				}
				else if (p.cod < p_l[i].primNo->info.cod || (p.cod > p_l[i].primNo->info.cod && p.cod < p_l[i].ultimoNo->info.cod) && p_l[i].tam >= TAM_MAX_LISTA)//o novo valor esta entre o primeiro e o ultimo da lista e nao tem espaço na lista
				{
					//printf("%d\n",p.cod );
					No_lista *aux = p_l[i].primNo;
					while (aux->prox != p_l[i].ultimoNo) {
						aux = aux->prox;
						if(aux->prox == NULL) //AUX PROX NULL!!!
							break;
					} //na saida do while, aux estara apontando para o penultimo da lista
					if(p_l[i+1].tam < TAM_MAX_LISTA){
						inserePaciente(&p_l[i+1], *p_l[i].ultimoNo);	//o ultimo da lista i sera inserido no primeiro da lista i+1
						aux->prox = p_l[i+1].primNo;
						p_l[i].ultimoNo = aux;
						p_l[i].tam--;
						inserePaciente(&p_l[i], p);
					} else {
						for (j = TAM_VET_CAB-1; j >= i; j--)
						{
							if(p_l[j].primNo == NULL){

							} else {
								if(p_l[j].tam == TAM_MAX_LISTA){ //lista cheia, o ultimo vai pra lista de baixo e diminuir o tamanho
									inserePaciente(&p_l[j+1],p_l[j].ultimoNo->info);
									aux = p_l[j].primNo;
									while (aux->prox != p_l[j].ultimoNo) {
											aux = aux->prox;
									if(aux->prox == NULL) //AUX PROX NULL!!!
										break;
									}
									aux->prox = p_l[j+1].primNo;

									p_l[j].ultimoNo = aux;
									p_l[j].tam--;
								} else {
									//é menor que TAM_MAX_LISTA
									if(j!=0 && p_l[j-1].ultimoNo->info.cod > p.cod){ //não posso puxar de -1 pq não existe e não posso puxar um menor que eu
										inserePaciente(&p_l[j],p_l[j-1].ultimoNo->info);
										aux = p_l[j-1].primNo;
										while (aux->prox != p_l[j-1].ultimoNo) {
											aux = aux->prox;
										if(aux->prox == NULL) //AUX PROX NULL!!!
											break;
										}
										aux->prox = p_l[j].primNo;

										p_l[j-1].ultimoNo = aux;
										p_l[j-1].tam--;
									}
									
								}
							}
						}
						inserePaciente(&p_l[i], p);
					}
					
					
					flag++;
					return 1;
				}
			}
		}
	}
	else
	{	
		printf(LISTAS_CHEIAS);
		return 0;
	}
}

// insere um paciente na lista. Retorna 1 se inseriu com sucesso ou 0 caso contrario
int inserePaciente(Lista *p_l, Paciente p) {
	int i;
	No_lista *novo;
	novo = (No_lista*) malloc(sizeof(No_lista));
	No_lista *aux2 = p_l->primNo;
	novo->info = p;
		
	if(aux2 == NULL)//lista vazia
	{
		novo->prox = NULL;
		aux2 =  novo;
		p_l->primNo = aux2;
		p_l->ultimoNo = aux2;
		p_l->tam++;
	}
	else
	{
		if (p_l->tam == 1)//tem só 1 elemento
		{

			if(novo->info.cod > aux2->info.cod)
			{

				aux2->prox = novo;
				novo->prox = NULL;
				p_l->ultimoNo = novo;
				p_l->tam++;

			}
			else
			{
				novo->prox = aux2;
				aux2->prox = NULL;
				p_l->ultimoNo = aux2;
				p_l->primNo = novo;
				p_l->tam++;
			}
		}
		else
		{
			if (!cheio(p_l)) {


				if (p.cod < p_l->primNo->info.cod) { 	//inseriR no inicio
					novo->prox = p_l->primNo;
					p_l->primNo = novo;
					p_l->tam++;
				}
				else if (p_l->ultimoNo->info.cod < p.cod) {		//inserir no fim
						novo->prox = p_l->ultimoNo->prox;	//o ultimo agora devera apontar para o proximo da outra lista
						p_l->ultimoNo->prox = novo;
						p_l->ultimoNo= novo;
						p_l->tam++;
				}
				else
				{
					No_lista *aux = p_l->primNo;
					while(aux->prox->info.cod < p.cod)
						aux = aux->prox;

					novo->prox = aux->prox;		//inserir no meio
					aux->prox = novo;
					p_l->tam++;
				}
			}
		}
	}
}


int auxRemove(Lista *p_l, Paciente *p){

	int i, j;
	No_lista *aux;
	No_lista *aux2;
	i = 0;
	if(p_l[i].primNo == NULL)
	{
		printf (PACIENTE_N_ENCONTRADO);
		return 0;
	}
	else
	{
		while(p_l[i].primNo != NULL && p_l[i].tam <= TAM_VET_CAB)
		{
			if(p_l[i].tam < TAM_MAX_LISTA) //tem espaço na lista, logo, o primNo do i+1 eh null
			{
				removePaciente(&p_l[i], p);
				return 1;
				flag--;
			}
			else if ((p->cod >= p_l[i].primNo->info.cod && p->cod <= p_l[i].ultimoNo->info.cod) && p_l[i].tam >= TAM_MAX_LISTA)//o valor a ser removido esta entre o primeiro e o ultimo da lista e nao tem espaço na lista
			{
				removePaciente(&p_l[i], p);
				//if (removePaciente(&p_l[i], p)) {
					j = i;
				while(p_l[j+1].primNo != NULL && p_l[j].tam < TAM_MAX_LISTA && j < TAM_VET_CAB-1) {
						inserePaciente(&p_l[j],p_l[j+1].primNo->info);
						aux = p_l[j+1].primNo;
						if(p_l[j+1].tam != 1) {
							p_l[j+1].primNo = aux->prox;
						} else {
							p_l[j+1].primNo = NULL;
						}
						free(aux);
						p_l[j+1].tam--;
				j++;
				}
				flag--;
				if(p_l[j-1].ultimoNo != NULL){
					p_l[j-1].ultimoNo->prox = NULL; //forco o ultimo no de tudo ser nulo
				}
				return 1;
				//}	//o codigo sera removido da lista
				
			}
			i++;
		}
	}

	printf (PACIENTE_N_ENCONTRADO);
		return 0;
}

// remove um paciente com codigo determinado e insere o nome do paciente.
int removePaciente(Lista *p_l, Paciente *p) {

	No_lista *aux;
	No_lista *aux2;
	int i = 0;
	int flag1 = 0;
	//printf("%d\n",p->cod );
	while(i < p_l->tam)
	{
		aux =  p_l->primNo;

		if(aux->prox != NULL)
			aux2 = aux->prox;

		//Primeiro elemento
		if(aux->info.cod == p->cod)
		{
			printf (PACIENTE_REMOVIDO, aux->info.nome);
			p_l->primNo = aux->prox;
			*p = aux->info;
			free(aux);
			p_l->tam--;
			flag1++;
			return 1;
		}
		else
			if (aux->prox == NULL)
			{
				printf (PACIENTE_N_ENCONTRADO);
				return 0;
			}
			else
			{
				//remover do meio
				while (aux2->prox != NULL) {
					if (aux2->info.cod == p->cod) {
						printf (PACIENTE_REMOVIDO, aux2->info.nome);
						aux->prox = aux2->prox;
						*p = aux->info;
						free(aux2);
						p_l->tam--;
						flag1++;
						return 1;
					} else {
						aux = aux->prox;
						aux2 = aux2->prox;
					}
				}

				if (aux2->prox == NULL) {
					if (aux2->info.cod == p->cod) {		//saindo do while (aux->prox == NULL), logo, verificar a informacao de aux
							printf (PACIENTE_REMOVIDO, aux2->info.nome);
							aux->prox = aux2->prox;
							*p = aux2->info;
							free(aux2);
							p_l->tam--;
							p_l->ultimoNo = aux;
							flag1++;
							return 1;
					}
				}
			}
			i++;
	}//while
	if (flag1 == 0)
	{
		printf (PACIENTE_N_ENCONTRADO);
		return 0;
	}

}

// busca um paciente na lista e retorna 1 se ja existe
int buscaPaciente(Lista *p_l, Paciente p) {

	int i = 0, cont = 0;

	if (flag == 0 || p_l[i].primNo == NULL) {
		printf (PACIENTE_N_ENCONTRADO);
		return 0;
	} else {

		while (p_l[i].ultimoNo != NULL && p_l[i].tam <= TAM_VET_CAB) {
			No_lista *aux = p_l[i].primNo;
			while (aux->prox != NULL) {
				if (aux->info.cod == p.cod) {
					printf("%s\n", aux->info.nome);
					return 1;
				} else {
					aux = aux->prox;
				}
			}

			if (aux->prox == NULL) {
				if (aux->info.cod == p.cod) {		//saindo do while (aux->prox == NULL), logo, verificar a informacao de aux
						printf("%s\n", aux->info.nome);
						return 1;
				} 
			}
			i++;
		}
		
		printf (PACIENTE_N_ENCONTRADO);
		return 0;
	}

}

// remove todos os pacientes da lista
void limpaPaciente(Lista *p_l) {

	No_lista *aux = p_l->primNo;
	No_lista *aux2 = p_l->primNo;

	while (aux != NULL) {
		aux2 = aux->prox;
		free(aux);
		aux = aux2;
	}

	p_l->primNo = NULL;
	p_l->ultimoNo = NULL;

}

// Lista os elementos em ordem de posição na lista (ordem crescente)
void listar_elementos(Lista vet_cab[]) {

	int i = 0;
	
	while(vet_cab[i].primNo != NULL && i < TAM_MAX_LISTA)
	{
	No_lista *aux2 = vet_cab[i].primNo;
		while (aux2 != vet_cab[i].ultimoNo) {
			printf("%d ", aux2->info.cod);
			printf("%s\n", aux2->info.nome);
			aux2 = aux2->prox;

		}
		if(aux2 == vet_cab[i].ultimoNo)
		{
			printf("%d ", aux2->info.cod);
			printf("%s\n", aux2->info.nome);
		}
		i++;
	}

}

// busca um paciente na lista e retorna 1 se ja existe
int verificaPaciente(Lista *p_l, Paciente p) {

	int i = 0;

	while (p_l[i].ultimoNo != NULL && i < TAM_MAX_LISTA && p_l[i].primNo != NULL) {
		No_lista *aux = p_l[i].primNo;
		while (aux != p_l[i].ultimoNo) {
			if (aux->info.cod == p.cod) {
				return 1;
			} else {
				aux = aux->prox;
			}
		}
		if(aux == p_l[i].ultimoNo){
			if (aux->info.cod == p.cod) 
				return 1;
		}

		i++;
	}
	return 0;	
}


int main() {
    Lista vet_cab[TAM_VET_CAB];

    int opc;
	int qntd = 0, i;
	Paciente p;
    inic_vet(vet_cab);

    scanf("%d", &opc);
    do {
        switch (opc) {
            case 1:
					scanf("%d", &qntd);
                for (i = 0; i < qntd; i++)
                {
                   	scanf("%d",&p.cod);
                	scanf("%*c%[^\n]",p.nome);
                	if (vet_cab->primNo == NULL) {
                		auxInsere(vet_cab, p);
                		printf(INSERIDO);
                	} else {
	                	if (verificaPaciente(vet_cab, p)) {
	                		printf(CODIGO_USADO);
	                	} else {
	                		if(auxInsere(vet_cab, p)){
	                			printf(INSERIDO);
	                		}else{

	                		}
	                	}
	                }
                }
            	break;
            case 2:
                scanf("%d",&p.cod);
	            auxRemove(vet_cab, &p);
                break;
            case 3:
            	scanf ("%d", &p.cod);
              	buscaPaciente(vet_cab, p);
              	break;
            case 4:
            	for (i = 0; i < TAM_VET_CAB; i++) {
            		if (vet_cab[i].primNo != NULL) {
            			limpaPaciente(vet_cab);
            		}
            	}
            	flag = 0;
            	printf (LISTAS_APAGADAS);
            	inic_vet(vet_cab);

                break;
            case 5:
            //printf("vou listar\n");
                listar_elementos(vet_cab);
                break;
			case 0:
                return 0;
        }
        scanf("%d", &opc);
        qntd = 0;
    } while (opc != 0);

    return 0;
}