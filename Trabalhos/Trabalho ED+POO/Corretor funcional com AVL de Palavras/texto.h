/*
texto.h
Este m�dulo desenvolve a classe texto, que manipula o texto a ser corrigido, fornecido
pelo usu�rio. Para isso existem fun��es para ler e gravar o texto em arquivos externos.

Dados de entrada: arquivo texto e op��es para corre��o das palavras
Dados de saida: palavras com erro e texto corrigido

Ultima modifica��o: 10/06/2015
*/

#include <iostream>
#include <string>
#include "palavra.h"

using namespace std;

class Texto {
	private:
		string nomeArq; // Nome do arquivo onde sera escrito e gravado
		Palavra palavra[10000]; // Vetor de Palavras
		int indice; // Indice de onde estamos no texto
	public:
		Texto( string ); // Construtor que define o nome do arquivo que contem o texto
		~Texto();
		void carregarTexto(); // Carrega Palavras no vetor do arquivo original
		void percorrerTexto(); // Incrementa o indice de onde estamos no texto em um
		int alterarPalavra(string); // Altera a palavra errada para uma nova
		void gravarTexto(string); // Escreve as palavras atualizadas no arquivo especificado
		string getPalavra(); // Retorna string contendo a palavra
		void contextoErro(); // Apresenta o contexto em que o erro aconteceu, ou seja, printa a palavra anterior a palavra errada e a seguinte
};
