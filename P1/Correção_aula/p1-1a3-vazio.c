#include<stdio.h>
#include<stdlib.h>

typedef int elem_t;
typedef struct No{
  elem_t info;
  struct No *prox;
  struct No *ant;
} No_Lista;


void inicLista(No_Lista *p_l){
	p_l->prox = NULL;
	p_l->info = -1;
}

void inicListaD(No_Lista **p_l){
	*p_l = NULL;
}

/* Insere um elemento no inicio da lista */
void insereInicio(No_Lista *p_l, elem_t e){
	No_Lista *novo;
	novo = malloc (sizeof(No_Lista));
	novo->info = e;
	novo->prox = p_l->prox;
	p_l->prox = novo;	
}

void insereInicioD(No_Lista **p_l, elem_t e){
	No_Lista *novo;
	novo = malloc (sizeof(No_Lista));
	novo->info = e;
	novo->ant = NULL;
	if (*p_l == NULL)
	  novo->prox = NULL;
	else {
	  novo->prox = *p_l;
	  (*p_l)->ant = novo;	
	}
	*p_l = novo;
}

void exibe(No_Lista *p_l){
	while(p_l != NULL){
		printf("%d ", p_l->info);
		p_l = p_l->prox;
	}
}

void removeMaior1 (No_Lista* p_l, int *x){
}

void removeMaior3 (No_Lista** p_l, int *x){
}

void imprimeRec(No_Lista *p_l){
}

void imprimeIt(No_Lista *p_l){
}

int main(){
  No_Lista ls;
  No_Lista *ld;
  int i;


  inicLista(&ls);
  inicListaD(&ld);

  insereInicio(&ls, 7);
  insereInicio(&ls, 4);
  insereInicio(&ls, 5);
  insereInicio(&ls, 3);
  insereInicio(&ls, 6);
  exibe(&ls);
  printf("\n");
  removeMaior1(&ls, &i);
  printf("\n%d\n", i);
  exibe(&ls);
  removeMaior1(&ls, &i);
  printf("\n%d\n", i);
  exibe(&ls);
  removeMaior1(&ls, &i);
  printf("\n%d\n", i);
  exibe(&ls);
  removeMaior1(&ls, &i);
  printf("\n%d\n", i);
  exibe(&ls);
  removeMaior1(&ls, &i);
  printf("\n%d\n", i);
  exibe(&ls);
  removeMaior1(&ls, &i);
  printf("\n%d\n", i);


  printf("\n\n");
  insereInicioD(&ld, 7);
  insereInicioD(&ld, 4);
  insereInicioD(&ld, 5);
  insereInicioD(&ld, 3);
  insereInicioD(&ld, 6);
  exibe(ld);
  printf("\n");
  imprimeRec(ld);
  printf("\n");
  imprimeIt(ld);
  printf("\n");

  removeMaior3(&ld, &i);
  printf("\n%d\n", i);
  exibe(ld);
  removeMaior3(&ld, &i);
  printf("\n%d\n", i);
  exibe(ld);
  removeMaior3(&ld, &i);
  printf("\n%d\n", i);
  exibe(ld);
  removeMaior3(&ld, &i);
  printf("\n%d\n", i);
  exibe(ld);
  removeMaior3(&ld, &i);
  printf("\n%d\n", i);
  exibe(ld);
  removeMaior3(&ld, &i);
  printf("\n%d\n", i);
  //exibe(ld);
  printf("\n\n");

  return 0;
}
