/*
dicionario.h
Este m�dulo desenvolve a classe dicionario, utilizada para armazenar cada palavra
ja cadastrada no arquivo dicionario em uma arvore do tipo AVL para tornar a busca
mais eficiente.

Dados de entrada: Palavra do arquivo de dicionario e palavra fornecida pelo usuario
Dados de saida: Dicionario com palavras adicionadas pelo usu�rio

Ultima modifica��o: 10/06/2015
*/

#include <iostream>
#include <string>
#include <fstream>
#include "dicionario.h"

using namespace std;
using std::ifstream;

//-----------------------PUBLIC--------------------------
//construtor
Dicionario::Dicionario(){
	T = NULL;
	carregarDic();
}

//destrutor
Dicionario::~Dicionario(){
	ofstream file("dic.txt");
	gravaDicionario( file, T );
	desaloca(T);
	T = NULL;
}

//carrega o arquivo de dicionario
void Dicionario::carregarDic() {
	ifstream fileDic( "dic.txt", ios::in );	//ios::in eh para gararntir que nao sera modificado
	if( fileDic.is_open() ) {
		string word;
		while( !fileDic.eof() ) { 
			fileDic >> word;
			Insere(word);
		}
	} else {
		cout << "Arquivo nao encontrado" << endl;
	}
}

 //inser��o de dados na arvore avl
void Dicionario::Insere(string n){
	insere(T, n);
}

//impress�o em ordem da arvore avl
void Dicionario::inOrder(){
	emOrdem(T);
}

void Dicionario::desaloca(){
	desaloca(T);
}

//busca palavras para verificar existencia da palavra
int Dicionario::busca(string n){
	if(busca(T, n))
		return 1;
		//cout<<"ACHOU"<<endl;
	else
		return 0;
		//cout<<"NAO ACHOU"<<endl;
}

//procura palavras semelhantes
void Dicionario::fornecerSemelhantes(string n) {
	fornecerSemelhantes(T, n);
}

//-----------------------PRIVATE--------------------------

//impress�o em ordem alfabetica da arvore
void Dicionario::emOrdem(No *x){
	if (x != NULL){
    	emOrdem(x->esq);  
    	cout << x->info << endl;  
		emOrdem	(x->dir);  
	}
}

//verifica o balanceamento de cada n� da arvore
void Dicionario::balanceia(No *&x){
	if(x->b == 2){
            if(x->esq->b == -1)
                LR(x->esq);
            else
            	RR(x);
	}
	else if(x->b == -2){
            if(x->dir->b == 1)
				RL(x->dir);
			else
            	LL(x);
	}
}

//atualiza o peso de cada n� da arvore
void Dicionario::atualiza(No *&x){
	int hesq, hdir;
	if(x->esq)
		hesq=x->esq->h;
	else
		hesq=0;
	if(x->dir)
		hdir=x->dir->h;
	else
		hdir=0;
	x->b = hesq-hdir;
	

}

//rota��o LL para o caso de desbalanceamento da arvore
void Dicionario::LL(No *&x){
	No *aux = x;  
	x = x->dir;  
    aux->dir = x->esq;  
    x->esq = aux;  
    atualiza(aux);  
    atualiza(x);
}

//rota��o RR para o caso de desbalanceamento da arvore
void Dicionario::RR(No *&x){
	No *aux = x;  
    x = x->esq;  
    aux->esq = x->dir;  
	x->dir = aux;  
    atualiza(aux);  
    atualiza(x);  
}

//rota��o LR para o caso de desbalanceamento da arvore
void Dicionario::LR(No *&x) {  //lembrar que b eh o fator de balanceamento
  No *c = x;
  No *a = c->esq;
  No *b = a->dir;
  c->esq = b->dir;
  a->dir = b->esq;
  b->esq = a;
  b->dir = c;
  atualiza(a);
  atualiza(c);
  atualiza(b);
  b->b = 0;
  x = b;
}

//rota��o RL para o caso de desbalanceamento da arvore
void Dicionario::RL(No *&x) {
  No *a = x;
  No *c = a->dir;
  No *b = c->esq;
  c->esq = b->dir;
  a->dir = b->esq;
  b->esq = a;
  b->dir = c;
  atualiza(a);  //a e c sao filhos de b, por isso nessa ordem
  atualiza(c);
  atualiza(b);
  b->b = 0;
  x = b;
}

// Desaloca arvore
void Dicionario::desaloca(No *x){
	if (x!=NULL){
		desaloca(x->esq);
		desaloca(x->dir);
		delete x;
	}		
}

//inser�ao de dados na arvore
void Dicionario::insere(No *&x, string el){
	if (!x) {  
    	x = new No();  
        x->info = el;  
    }else{
		if(x->info == el)
			return;  
    	if (x->info > el) 
			insere(x->esq, el);  
        else 
		insere(x->dir, el);  
        atualiza(x);  
        balanceia(x);  
   }  
}

//busca uma palavra na arvore
int Dicionario::busca(No *x, string el){
	if(x == NULL)
		return 0;
	if(x->info == el)
		return 1;
	else{
		return busca(x->esq, el) + busca(x->dir, el);
	}
}

//fornece palavras semelhantes
void Dicionario::fornecerSemelhantes(No *x, string el) {

	int cont = 0;
	int i;
	//if(strncmp(const char * str1, const char * str2, size_t num));

	if(x == NULL){
		cout<< endl;
		return;
	}
	for (i = 0; i < 2; i++) {
		if (x->info[i] == el[i])
			cont++;
	}
	if(cont == 2){
		cout<< x->info << endl;		
	}
	
	if (x->esq != NULL)
		fornecerSemelhantes(x->esq, el);
	if (x->dir != NULL)
	fornecerSemelhantes(x->dir, el);
}

// Grava dicionario
void Dicionario::gravaDicionario( ofstream &file, No *&x ) {
	if( x != NULL ) {
		file << x->info << endl;
		gravaDicionario( file ,x->dir );
		gravaDicionario( file, x->esq );
	}
}