/*
dicionario.h
Este m�dulo desenvolve a classe dicionario, utilizada para armazenar cada palavra
ja cadastrada no arquivo dicionario em uma arvore do tipo AVL para tornar a busca
mais eficiente.

Dados de entrada: Palavra do arquivo de dicionario e palavra fornecida pelo usuario
Dados de saida: Dicionario com palavras adicionadas pelo usu�rio

Ultima modifica��o: 10/06/2015
*/

#include <iostream>
#include <string>
using namespace std; 

class Dicionario {
	public:
		Dicionario();
		~Dicionario();
		void Insere(string n); //inser��o de dados na arvore avl
		void inOrder(); //impress�o em ordem da arvore avl
		void desaloca(); 
		int busca(string n); //busca palavras para verificar existencia da palavra
		void fornecerSemelhantes(string n); //procura palavras semelhantes
		void carregarDic(); //carrega o arquivo de dicionario
	private:
		//classe de cada n� da arvore
		class No{
			public:
				No *esq, *dir; //ponteiros para filhos
				string info; //conteudo
				int h; //altura
				int b; //balanceamento
		};
		void emOrdem(No *x); //impress�o em ordem alfabetica da arvore
		void balanceia(No *&x); //verifica o balanceamento de cada n� da arvore
		void atualiza(No *&x); //atualiza o peso de cada n� da arvore
		void LL(No *&x); //rota��o LL para o caso de desbalanceamento da arvore
		void RR(No *&x); //rota��o RR para o caso de desbalanceamento da arvore
		void LR(No *&x); //rota��o LR para o caso de desbalanceamento da arvore
		void RL(No *&x); //rota��o RL para o caso de desbalanceamento da arvore
		void desaloca(No *x);
		void insere(No *&x, string el); //inser�ao de dados na arvore
		int busca(No *x, string el); //busca uma palavra na arvore
		void fornecerSemelhantes(No *x, string el); //fornece palavras semelhantes
		void gravaDicionario( ofstream &file, No *&x );// Grava dicionario
		No *T; //N� da arvore
};

