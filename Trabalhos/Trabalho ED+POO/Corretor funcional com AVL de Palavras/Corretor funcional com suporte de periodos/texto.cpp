/*
texto.cpp
Este m�dulo desenvolve a classe texto, que manipula o texto a ser corrigido, fornecido
pelo usu�rio. Para isso existem fun��es para ler e gravar o texto em arquivos externos.

Dados de entrada: arquivo texto e op��es para corre��o das palavras
Dados de saida: palavras com erro e texto corrigido

Ultima modifica��o: 10/06/2015
*/

#include <cstdlib>
#include <fstream>
#include "texto.h"


// Construtor
Texto::Texto( string nome ) {
	nomeArq = nome;
	indice = 0;
	carregarTexto(); // Inicia o carregamente de texto
}

// Destrutor
Texto::~Texto() {
	nomeArq = "\0";
	indice = 0;
}

// Carrega Palavras no vetor do arquivo original
void Texto::carregarTexto() {
	//int flag = 0;
	ifstream file( (nomeArq.c_str()));		//Because the constructor for an ifstream takes a const char*, not a string pre-C++11.
	if( file.is_open() ) {
		string word;
		while( file >> word ) {
			static int i = 0; // static para nao ficar sobreescrevendo
			palavra[i++].setPalavra(word);
			if(file.peek() == '\n'){
				palavra[i - 1].setEnter(); // Tem um enter apos a palavra
			}
		}
	} else {
		cout << "Arquivo n�o encontrado." << endl;
		exit (EXIT_FAILURE);
	}
}

// Incrementa o indice de onde estamos no texto em um
void Texto::percorrerTexto() {
	indice++;
}

// Altera a palavra errada para uma nova
int Texto::alterarPalavra( string novo ) {

	palavra[indice].setPalavra(novo);

}

// Escreve as palavras atualizadas no arquivo especificado
void Texto::gravarTexto(string arqSaida) {

	ofstream file(arqSaida.c_str()); //Because the constructor for an ifstream takes a const char*, not a string pre-C++11.
	if( file.is_open() ) {
		int i = 0;
		while( palavra[i].getPalavra() != "\0" ) {

			file << palavra[i++].getPalavra();
			if( palavra[i - 1].getPeriodo() != '\0' ) // Verifica se tem periodo
				file << palavra[i - 1].getPeriodo();
			if( palavra[i - 1].getEnter() ) {// Verifica se tem enter
				file << endl;
			} else if( palavra[i].getPalavra() != "\0" ) {// Verifica se tem proxima palavra para poder printar espaco
				file << ' ';
			}
		}
	} else {
		 cout << "Erro na hora de gravar o arquivo." << endl;
	}

}

// Retorna string contendo a palavra
string Texto::getPalavra() {
	return palavra[indice].getPalavra();
}

// Apresenta o contexto em que o erro aconteceu, ou seja, printa a palavra anterior a palavra errada e a seguinte
void Texto::contextoErro() {

	cout << "Contexto:" << endl;
	if (indice == 0) { // Caso seja a primeira palavra
		cout << palavra[indice].getPalavra();
		if(palavra[indice].getPeriodo() != '\0')
			cout << palavra[indice].getPeriodo();
		cout << ' ' << palavra[indice + 1].getPalavra();
		if( palavra[indice + 1].getPeriodo() != '\0' )
			cout << palavra[indice + 1].getPeriodo();
		cout << endl;
	} else if (indice == 9999) { // Caso seja a ultima palavra
		cout << palavra[indice - 1].getPalavra();
		if( palavra[indice - 1].getPeriodo() != '\0' )
			cout << palavra[indice - 1].getPeriodo();
		cout << ' ' << palavra[indice].getPalavra();
		if( palavra[indice].getPeriodo() != '\0' )
			cout << palavra[indice].getPeriodo();
		cout << endl;
	} else { // Caso contrario
		cout << palavra[indice - 1].getPalavra();
		if( palavra[indice - 1].getPeriodo() != '\0' )
			cout << palavra[indice - 1].getPeriodo();
		cout << ' ' << palavra[indice].getPalavra();
		if( palavra[indice].getPeriodo() != '\0' )
			cout << palavra[indice].getPeriodo();
		cout << ' ' << palavra[indice + 1].getPalavra();
		if( palavra[indice + 1].getPeriodo() != '\0' )
			cout << palavra[indice + 1].getPeriodo();
		cout << endl;
	}
	
}

/* ostream& operator<<( ostream & o, const string &operand2) {

	o <<  << ", " <<  << <<endl;
	return o;

}*/