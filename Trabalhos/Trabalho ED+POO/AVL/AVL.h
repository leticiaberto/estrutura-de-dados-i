#include <iostream>
#include <string>

using namespace std; 

class AVL {
	public:
		AVL();
		~AVL();
		void Insere(string n); //inserção de dados na arvore avl
		void inOrder(); //impressão em ordem da arvore avl
		void desaloca(); 
		int busca(string n); //busca palavras para verificar existencia da palavra
	private:
		//classe de cada nó da arvore
		class No{
			public:
				No *esq, *dir; //ponteiros para filhos
				string info; //conteudo
				int h; //altura
				int b; //balanceamento
		};
		void emOrdem(No *x); //impressão em ordem alfabetica da arvore
		void balanceia(No *&x); //verifica o balanceamento de cada nó da arvore
		void atualiza(No *&x); //atualiza o peso de cada nó da arvore
		void LL(No *&x); //rotação LL para o caso de desbalanceamento da arvore
		void RR(No *&x); //rotação RR para o caso de desbalanceamento da arvore
		void LR(No *&x); //rotação LR para o caso de desbalanceamento da arvore
		void RL(No *&x); //rotação RL para o caso de desbalanceamento da arvore
		void desaloca(No *x);
		void insere(No *&x, string el); //inserçao de dados na arvore
		int busca(No *x, string el); //busca uma palavra na arvore
		No *T; //Nó da arvore
};


