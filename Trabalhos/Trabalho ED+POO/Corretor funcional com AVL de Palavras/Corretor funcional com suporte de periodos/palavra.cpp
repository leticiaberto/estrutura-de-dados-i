/*
palavra.cpp
Este m�dulo desenvolve a classe palavra, utilizado para armazenar cada palavra
do texto e tamb�m fornecer op��es para uma poss�vel corre��o da palavra se desejada.

Dados de entrada: Palavra do arquivo de texto
Dados de saida: Palavras para corre��o

Ultima modifica��o: 10/06/2015
*/

#include <cstring>
#include "palavra.h"
#include <algorithm>
#include <iterator>

Palavra::Palavra() {
	palavra = "\0";
	periodo = '\0';
	enter = 0;
}

bool Palavra::operator==( string palavra2 ) {
	if( palavra == palavra2 )
		return 1;
	else
		return 0;
}

// Retorna string da palavra
string Palavra::getPalavra() {
	return palavra;
}

// Retorna caractere do periodo
char Palavra::getPeriodo() {
	return periodo;
}

// Seta igual a 1 caso tenha um enter
void Palavra::setEnter() {
	enter = 1;
}

// Verifica se tem um enterou n�o
bool Palavra::getEnter() {
	return enter;
}

// Atualiza palavra
void Palavra::setPalavra( string word ) {

	//word[word.size() - 1] pega o ultimo caracter
	if( word[word.size() - 1] == '.' || word[word.size() - 1] == ',' || word[word.size() - 1] == '!' || word[word.size() - 1] == '?' ) { // Verifica qual e o ultimo caractere, nao funciona para espaco e enter pq o proprio file >> word ignora esses caracteres
		string result;
		remove_copy(word.begin(), word.end(), back_inserter(result), word[word.size() - 1]); // Pega a string sem o periodo
		palavra = result; // Salva a string sem o periodo
		periodo = word[word.size() - 1]; // Salva o periodo
	} else
		palavra = word; // Salva a string caso nao tenha periodo
}

// Retorna 1 caso seja semelhante, ou seja, caso tenha as duas primeiras letras iguais. 0 caso contrario
bool Palavra::verificarSemelhante( string palavra_dic ) {
	// Strncmp retorna 0 caso as duas primeiras letras sejam iguais
	if( strncmp( palavra.c_str(), palavra_dic.c_str(), 2 ) )
		return 0; // Nao sao semelhantes
	else
		return 1; // As duas palavras sejam semelhantes
}
