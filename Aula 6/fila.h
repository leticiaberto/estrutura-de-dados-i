/*
 * Implementacao de Fila Circular
 */

#ifndef FILA_H
#define FILA_H

#define MAX 10
typedef int elem_t;

typedef struct no{
  elem_t info[MAX];
  int head, tail;
  int qntd;
} Fila;

/* Inicializa uma Fila */
void inicFila(Fila *p_l);

/* Verifica se a Fila est� vazia ou nao */
int FilaVazia(Fila *p_l);

/* Verifica se a Fila est� cheia ou nao */
int FilaCheia(Fila *p_l);

/* Insere um elemento na Fila */
void insereFila(Fila *p_l, elem_t e);

/* Remove um elemento da Fila */
elem_t removeFila(Fila *p_l);

/* Remove todos os numeros da Fila */
void libera(Fila *p_l);

void FuraFila(Fila* p_l, elem_t e);
#endif
