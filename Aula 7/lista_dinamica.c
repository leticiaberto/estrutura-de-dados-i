/*
 * Implementacao de lista com n� cabe�a.
 */

#include"lista_dinamica.h"
#include<stdio.h>
#include<stdlib.h>

/* Inicializa uma lista */
void inicLista(Lista *p_l){
	p_l->prox = NULL;
}

/* Verifica se a lista est� vazia ou nao */
int listaVazia(Lista *p_l){
	if (p_l->prox == NULL)
		return 1;
	return 0;
}

/* Insere um elemento no inicio da lista */
void insereInicio(Lista *p_l, elem_t e){
	No_lista *novo;
	novo = malloc (sizeof(No_lista));
	novo->info = e;
	novo->prox = p_l->prox;
	p_l->prox = novo;	
}

/* Insere um elemento no final da lista */
void insereFim(Lista *p_l, elem_t e){
	No_lista *novo;
	
	if (p_l->prox == NULL){	
		novo = malloc (sizeof(No_lista));
		novo->info = e;
		novo->prox = NULL;
		p_l->prox = novo;	
	}
	else
		insereFim(p_l->prox, e);	
}

/* Insere um elemento na lista de maneira ordenada.
   Caso a lista nao esteja ordenada, ordena antes da insercao */
void insereOrdenado(Lista *p_l, elem_t e){
	No_lista *novo;
	novo = malloc (sizeof(No_lista));
	novo->info = e;

	if (!ordenada(p_l))
		ordena(p_l);
	while(p_l->prox != NULL && p_l->prox->info < e ){
		p_l = p_l->prox;
	}	
	novo->prox = p_l->prox;
	p_l->prox = novo;

}

/* Verifica se a lista esta ordenada */
int ordenada(Lista *p_l){
	int ord = 1;
	p_l = p_l->prox;
	if (p_l == NULL || p_l->prox == NULL)
		return 1;
	while(p_l->prox != NULL && ord) {
		if (p_l->info > p_l->prox->info)
			ord = 0;
		p_l = p_l->prox;
		
	}
	return ord;
}

/* Ordena a lista */
void ordena(Lista *p_l){

}

/* Remove o elemento que esta no inicio da lista.
   Retorna 0 caso a lista esteja vazia */
int removeInicio(Lista *p_l, elem_t *p_e){
	No_lista *aux;
	aux = p_l->prox;
	if (aux == NULL)
		return 0;
	p_l->prox = aux->prox; // p_l->prox->prox
	*p_e = aux->info;
	free (aux);
	return 1;
}

/* Remove o elemento que esta no final da lista.
   Retorna 0 caso a lista esteja vazia */
int removeFim(Lista *p_l, elem_t *p_e){
	No_lista *aux;
	aux = p_l->prox;
	if (aux == NULL)
		return 0;
	while(aux->prox != NULL) {
		p_l = aux;
		aux = aux->prox;
	}
	p_l->prox = NULL;
	*p_e = aux->info;
	free(aux);
	
	return 1;
}

/* Remove o numero de valor e.
   Retorna 0 caso este numero n�o tenha sido encontrado */
int removeValor(Lista *p_l, elem_t e){
	No_lista *aux;
	aux = p_l->prox;
	if (aux == NULL)
		return 0;
	while(aux->prox != NULL && aux->info != e){
		p_l = aux;
		aux = aux->prox;
	}
	if (aux->info == e){
		p_l->prox = aux->prox;
		free (aux);
		return 1;
	}
	return 0;
}

/* Inverte os elementos de uma lista */
void inverte(Lista *p_l){
	No_lista *aux1, *aux2, *aux3;
	aux1 = p_l->prox;
	if (aux1 == NULL) // lista vazia
		return;
	aux2 = aux1->prox;
	if (aux2 == NULL) // lista com um unico elemento
		return;
		
	aux1->prox = NULL;
	aux3 = aux2->prox;
	while(aux3 != NULL) {
		aux2->prox = aux1;
		aux1 = aux2;
		aux2 = aux3;
		aux3 = aux3->prox;
	}
	aux2->prox = aux1;
	p_l->prox = aux2;

}

/* Remove todos os numeros da lista */
void libera(Lista *p_l){
	No_lista *aux;
	aux = p_l->prox;
	while (aux != NULL) {
		p_l = aux->prox;
		free (aux);
		aux = p_l;
	}
	p_l->prox = NULL;
}

/* Exibe o conteudo da lista */
void exibe(Lista *p_l){
	while(p_l->prox != NULL){
		printf("%d ", p_l->prox->info);
		p_l = p_l->prox;
	}
}

