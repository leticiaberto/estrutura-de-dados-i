/*
corretor.cpp
Este m�dulo desenvolve a classe corretor, esta classe verifica os erros no texto,
fornecendo op��es de corre��o do texto, permitindo ao usu�rio corrigir ou n�o
o erro do texto.

Dados de entrada: palavra do texto, palavras do dicionario palavra do usu�rio
Dados de saida: Palavra corrigida ou palavra com erro ignorada pelo usu�rio

Ultima modifica��o: 10/06/2015
*/

#include "corretor.h"

// Corretor
Corretor::Corretor(string nomeArq) {
	//Texto texto(nomeArq);
	texto = new Texto(nomeArq);
	dicionario = new Dicionario;
	for( int i = 0 ; i < 10000 ; i++ ) {
		listaErros[i] = "\0";
		quantErros[i] = 0;
	}
}

// Destrutor
Corretor::~Corretor() {
	delete texto;
	delete dicionario;
}

// Corrigir palavra errada
void Corretor::corrigirErro(string correta) {
	texto->alterarPalavra(correta);
}

// Seleciona palavras semelhantes
void Corretor::selecionarSemelhante(string palavra) {		//listando para o usuario e este digita qual ele quer
	dicionario->fornecerSemelhantes(palavra);
}

// Adiciona palavra ao dicionario
void Corretor::adicionarDicionario(string palavra) {
	// Chama metodo do dicionario para incluir palavra
	dicionario->Insere(palavra);
}

// Percorre o texto verificando se a palavra esta errada. Inicaliza tambem um menu para corrigir o erro
void Corretor::verificaErro() {
	string palavra;
	int i;
	int opcMenu;
	int flag = 0;

	palavra = texto->getPalavra();
	//if (palavra != "\n" && palavra != " ") {
		while( palavra != "\0" ) {
			flag = 0;
			// Verifica se a palavra n�o exisite no dicionario
			if(!(dicionario->busca(palavra))) {		//DICIONARIO NAO TEM CONSULTAR PALAVRA
				// Adiciona palavra ou verifica se ja existe a palavra na lista de erros, incrementa a quantidade de erros nessa palavra em um
				for(i = 0 ; listaErros[i] != palavra && listaErros[i] != "\0" ; i++);
				if(listaErros[i] != palavra)
					listaErros[i] = palavra;
				quantErros[i]++;
				
				// Chama metodo que printa o contexto do erro, ou seja, a palavra anterior ao erro, a palavra errada e a palavra seguinte ao erro
				cout << "Erro: " << palavra << endl;
				texto->contextoErro();
				
				// Inicializa menu de erros
				cout << "Menu:" << endl << "1 - Corrigir Palavra" << endl << "2 - Ignorar Palavra" << endl << "3 - Selecionar Semelhante" << endl << "4 - Adicionar Palavra ao Dicionario" << endl;
				
				string correta; // String correta da palavra
				string arrumei;
				cin >> opcMenu;
				switch( opcMenu ) {
					case 1:
						// Corrigir Palavra
						flag = 1;
						cout << "Digite a palavra correta: ";
						cin >> correta;
						corrigirErro(correta);
						break;
					case 2:
						// Ignorar erro, so printa, pois o proprio metodo ja muda para a pr�xima palavra apos o menu
						cout << "Erro ignorado." << endl;
						break;
					case 3:
						// Selecionar palavra semelhante
						selecionarSemelhante(palavra);
						cout << "Escolha uma das palavras a ser trocada: ";
						cin >> arrumei;
						texto->alterarPalavra(arrumei);
						break;
					case 4:
						// Adicionar palavra ao dicionario
						cout << "Palavra adicionada ao Dicionario." << endl;
						adicionarDicionario(palavra);
						break;
				}
				
			}
			
			// Percorre o texto, indo para a pr�xima palavra
			if (flag == 0) //para verificar novamente a palavra corrigida
				texto->percorrerTexto();

			palavra = texto->getPalavra();

		} // Fim while

	cout << "Mostrar lista de erros? (1 - Sim/2 - Nao)" << endl;
	int opc;
	cin >> opc;
	if( opc ) {
		if (listaErros[0] != "\0") {
			for( i = 0 ; listaErros[i] != palavra && listaErros[i] != "\0" ; i++ ) {
				cout << listaErros[i] << ": " << quantErros[i] << endl;
			}
		} else 
			cout << "Nao existem erros." << endl;
	}

	cout << "Digite o nome do arquivo de saida: ";
	string arqSaida;
	cin >> arqSaida;
	texto->gravarTexto(arqSaida);
}
