/*
 *  Árvore AVL (Adelson-Velskii e Landis)
 *
 *  Fator de balanceamento = hdir - hesq
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include "avl.h"

int verifica_AVL(Arvore t){
	if (t == NULL)
		return 1;
	if (t->bal < -1 ||t->bal > 1 || )
		return 0;
	return verfica_AVL(t->esq) && verifica_AVL(t->dir);
}

void inorder(Arvore t){
	if (t != NULL){
		inorder(t->esq);
		printf("%d ", t->info);
		inorder(t->dir);
	}
}

int altura(Arvore t){
  int e, d;
  if (t == NULL)
    return 0;
  e = altura(t->esq);
  d = altura(t->dir);
  return e > d ? e + 1 : d + 1;
}

No *arv(int v, No* esq, No* dir){
  No *novo;
  novo = malloc(sizeof(No));
  novo->v = v;
  novo->bal = altura(esq) - altura(dir);
  novo->esq = esq;
  novo->dir = dir;
  return novo;
}

void LL(No** r) {
  No* b = *r;
  No* a = b->esq;
  b->esq = a->dir;
  a->dir = b;
  a->bal = 0;
  b->bal = 0;
  *r = a;
}

void RR(No** r) {
  No* a = *r;
  No* b = a->dir;
  a->dir = b->esq;
  b->esq = a;
  a->bal = 0;
  b->bal = 0;
  *r = b;
}

void LR(No** r) {
  No *c = *r;
  No *a = c->esq;
  No *b = a->dir;
  c->esq = b->dir;
  a->dir = b->esq;
  b->esq = a;
  b->dir = c;
  switch(b->bal) {
  case -1:
    a->bal = 0;
    c->bal = 1;
    break;
  case 0:
    a->bal = 0;
    c->bal = 0;
    break;
  case +1:
    a->bal = -1;
    c->bal = 0;
    break;
  }
  b->bal = 0;
  *r = b;
}

void RL(No** r) {
  No *a = *r;
  No *c = a->dir;
  No *b = c->esq;
  c->esq = b->dir;
  a->dir = b->esq;
  b->esq = a;
  b->dir = c;
  switch(b->bal) {
  case -1:
    a->bal = 0;
    c->bal = 1;
    break;
  case 0:
    a->bal = 0;
    c->bal = 0;
    break;
  case +1:
    a->bal = -1;
    c->bal = 0;
    break;
  }
  b->bal = 0;
  *r = b;  
}


/* *cresceu indica se a árvore cresceu 
   após a inserção */
int aux_insere(No** t, int chave, int *cresceu) {
  if (*t == NULL) {
    *t = cria(chave, NULL, NULL);
    *cresceu = 1;
    return 1;
  }
    
  if (chave == (*t)->info) 
    return 0;

  if (chave < (*t)->info) {
    if (aux_insere(&(*t)->esq, chave, cresceu)) {
      if (*cresceu) {
	switch ((*t)->bal) {
	case -1:
	  if ((*t)->esq->bal == -1)
	    LL(t);
	  else
	    LR(t);
	  *cresceu = 0;
	  break;
	case 0:
	  (*t)->bal = -1;
	  *cresceu = 1;
	  break;
	case +1:
	  (*t)->bal = 0;
	  *cresceu = 0;
	  break;
	}
      }
      return 1;
    }
    else
      return 0;
  }
  
  if (aux_insere(&(*t)->dir, chave, cresceu)) {
      if (*cresceu) {
	switch ((*t)->bal) {
	case -1:
	  (*t)->bal = 0;
	  *cresceu = 0;
	  break;
	case 0:
	  (*t)->bal = +1;
	  *cresceu = 1;
	  break;
	case +1:
	  if ((*t)->dir->bal == +1)
	    RR(t);
	  else
	    RL(t);
	  *cresceu = 0;
	  break;
	}
      }
      return 1;
    }
    else
      return 0;

 
}

/* Retorna 1 se inseriu ou 0 se 
   o elemento ẽ repetido. */
int insere(No **t, int chave) {
  int cresceu;
  return aux_insere(t, chave, &cresceu);
}

void libera(Arvore* t){
}
