/*
dicionario.h
Este módulo desenvolve a classe dicionario, utilizada para armazenar cada palavra
ja cadastrada no arquivo dicionario em uma arvore do tipo AVL para tornar a busca
mais eficiente.

Dados de entrada: Palavra do arquivo de dicionario e palavra fornecida pelo usuario
Dados de saida: Dicionario com palavras adicionadas pelo usuário

Ultima modificação: 10/06/2015
*/

#include <iostream>
#include <string>
#include <fstream>
#include "dicionario.h"

using namespace std;
using std::ifstream;

//-----------------------PUBLIC--------------------------
//construtor
Dicionario::Dicionario(){
	T = NULL;
	carregarDic();
}

//destrutor
Dicionario::~Dicionario(){
	ofstream file("dic.txt");
	gravaDicionario( file, T );
	desaloca(T);
	T = NULL;
}

//carrega o arquivo de dicionario
void Dicionario::carregarDic() {
	ifstream fileDic( "dic.txt", ios::in );	//ios::in eh para gararntir que nao sera modificado
	if( fileDic.is_open() ) {
		string word;
		while( fileDic >> word ) { 
			Insere(word);
		}
	} else {
		cout << "Arquivo nao encontrado" << endl;
	}
}

 //inserção de dados na arvore avl
void Dicionario::Insere(string n){
	insere(T, n);
}

//impressão em ordem da arvore avl
void Dicionario::inOrder(){
	emOrdem(T);
}

void Dicionario::desaloca(){
	desaloca(T);
}

//busca palavras para verificar existencia da palavra
int Dicionario::busca(string n){
	if(busca(T, n))
		return 1;
		//cout<<"ACHOU"<<endl;
	else
		return 0;
		//cout<<"NAO ACHOU"<<endl;
}

//procura palavras semelhantes
void Dicionario::fornecerSemelhantes(string n) {
	fornecerSemelhantes(T, n);
}

//-----------------------PRIVATE--------------------------

//impressão em ordem alfabetica da arvore
void Dicionario::emOrdem(No *x){
	if (x != NULL){
    	emOrdem(x->esq);  
    	cout << (x->info).getPalavra() << endl;  
		emOrdem	(x->dir);  
	}
}

//verifica o balanceamento de cada nó da arvore
void Dicionario::balanceia(No *&x){
	if(x->b == 2){
            if(x->esq->b == -1)
                LR(x->esq);
            else
            	RR(x);
	}
	else if(x->b == -2){
            if(x->dir->b == 1)
				RL(x->dir);
			else
            	LL(x);
	}
}

//atualiza o peso de cada nó da arvore
void Dicionario::atualiza(No *&x){
	int hesq, hdir;
	if(x->esq)
		hesq=x->esq->h;
	else
		hesq=0;
	if(x->dir)
		hdir=x->dir->h;
	else
		hdir=0;
	x->b = hesq-hdir;
	

}

//rotação LL para o caso de desbalanceamento da arvore
void Dicionario::LL(No *&x){
	No *aux = x;  
	x = x->dir;  
    aux->dir = x->esq;  
    x->esq = aux;  
    atualiza(aux);  
    atualiza(x);
}

//rotação RR para o caso de desbalanceamento da arvore
void Dicionario::RR(No *&x){
	No *aux = x;  
    x = x->esq;  
    aux->esq = x->dir;  
	x->dir = aux;  
    atualiza(aux);  
    atualiza(x);  
}

//rotação LR para o caso de desbalanceamento da arvore
void Dicionario::LR(No *&x) {  //lembrar que b eh o fator de balanceamento
  No *c = x;
  No *a = c->esq;
  No *b = a->dir;
  c->esq = b->dir;
  a->dir = b->esq;
  b->esq = a;
  b->dir = c;
  atualiza(a);
  atualiza(c);
  atualiza(b);
  b->b = 0;
  x = b;
}

//rotação RL para o caso de desbalanceamento da arvore
void Dicionario::RL(No *&x) {
  No *a = x;
  No *c = a->dir;
  No *b = c->esq;
  c->esq = b->dir;
  a->dir = b->esq;
  b->esq = a;
  b->dir = c;
  atualiza(a);  //a e c sao filhos de b, por isso nessa ordem
  atualiza(c);
  atualiza(b);
  b->b = 0;
  x = b;
}

// Desaloca arvore
void Dicionario::desaloca(No *x){
	if (x!=NULL){
		desaloca(x->esq);
		desaloca(x->dir);
		delete x;
	}		
}

//inserçao de dados na arvore
void Dicionario::insere(No *&x, string el){
	if (!x) {  
    	x = new No();
        x->info.setPalavra(el);
    }else{
		if( x->info == el) {
			return;
		}
    	if ((x->info).getPalavra() > el) 
			insere(x->esq, el);  
        else 
		insere(x->dir, el);  
        atualiza(x);  
        balanceia(x);  
   }  
}

//busca uma palavra na arvore
int Dicionario::busca(No *x, string el){
	if(x == NULL)
		return 0;
	if( (x->info).getPalavra() > el)
		return busca(x->esq, el);
	else if( (x->info).getPalavra() < el){
		return busca(x->dir, el);
	} else if( x->info == el)
		return 1;
}


//fornece palavras semelhantes
void Dicionario::fornecerSemelhantes(No *x, string el) {

	int cont = 0;
	int i;

	if(x == NULL){
		cout<< endl;
		return;
	}
	
	if( x->info.verificarSemelhante(el) ){
		cout<< x->info.getPalavra() << endl;		
	}
	
	if (x->esq != NULL)
		fornecerSemelhantes(x->esq, el);
	if (x->dir != NULL)
	fornecerSemelhantes(x->dir, el);
}

// Grava dicionario
void Dicionario::gravaDicionario( ofstream &file, No *&x ) {
	if( x != NULL ) {
		gravaDicionario(file ,x->esq);
		file << x->info.getPalavra() << endl;
		gravaDicionario(file, x->dir);
	}
}
