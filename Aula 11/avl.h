/*
 *  Árvore AVL (Adelson-Velskii e Landis)
 *
 *  Fator de balanceamento = hdir - hesq
 *
 */

typedef struct no {
  int v;
  int bal;   /* hdir - hesq */
  struct no *esq, *dir;
} No;

typedef No * Arvore;

int altura(Arvore t);

No *arv(int v, No* esq, No* dir);

int verifica_AVL(Arvore t);

void inorder(Arvore t);

void LL(Arvore* t);

void RR(Arvore* t);

void LR(Arvore* t);

void RL(Arvore* t);

int insere (Arvore *t, int v, int * cresceu);

void libera(Arvore* t);
