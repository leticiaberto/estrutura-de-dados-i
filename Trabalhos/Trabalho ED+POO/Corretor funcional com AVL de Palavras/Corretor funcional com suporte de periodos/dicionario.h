/*
dicionario.h
Este módulo desenvolve a classe dicionario, utilizada para armazenar cada palavra
ja cadastrada no arquivo dicionario em uma arvore do tipo AVL para tornar a busca
mais eficiente.

Dados de entrada: Palavra do arquivo de dicionario e palavra fornecida pelo usuario
Dados de saida: Dicionario com palavras adicionadas pelo usuário

Ultima modificação: 10/06/2015
*/

#include <iostream>
#include <string>
#include "palavra.h"

using namespace std; 

class Dicionario {
	public:
		Dicionario();
		~Dicionario();
		void Insere(string n); //inserção de dados na arvore avl
		void inOrder(); //impressão em ordem da arvore avl
		void desaloca(); 
		int busca(string n); //busca palavras para verificar existencia da palavra
		void fornecerSemelhantes(string n); //procura palavras semelhantes
		void carregarDic(); //carrega o arquivo de dicionario
	private:
		//classe de cada nó da arvore
		class No{
			public:
				No *esq, *dir; //ponteiros para filhos
				Palavra info; //conteudo
				int h; //altura
				int b; //balanceamento
		};
		void emOrdem(No *x); //impressão em ordem alfabetica da arvore
		void balanceia(No *&x); //verifica o balanceamento de cada nó da arvore
		void atualiza(No *&x); //atualiza o peso de cada nó da arvore
		void LL(No *&x); //rotação LL para o caso de desbalanceamento da arvore
		void RR(No *&x); //rotação RR para o caso de desbalanceamento da arvore
		void LR(No *&x); //rotação LR para o caso de desbalanceamento da arvore
		void RL(No *&x); //rotação RL para o caso de desbalanceamento da arvore
		void desaloca(No *x);
		void insere(No *&x, string el); //inserçao de dados na arvore
		int busca(No *x, string el); //busca uma palavra na arvore
		void fornecerSemelhantes(No *x, string el); //fornece palavras semelhantes
		void gravaDicionario( ofstream &file, No *&x );// Grava dicionario
		No *T; //Nó da arvore
};


