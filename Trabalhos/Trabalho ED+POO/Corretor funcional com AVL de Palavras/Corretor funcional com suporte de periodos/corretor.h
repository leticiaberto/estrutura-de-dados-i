/*
corretor.h
Este m�dulo desenvolve a classe corretor, esta classe verifica os erros no texto,
fornecendo op��es de corre��o do texto, permitindo ao usu�rio corrigir ou n�o
o erro do texto.

Dados de entrada: palavra do texto, palavras do dicionario palavra do usu�rio
Dados de saida: Palavra corrigida ou palavra com erro ignorada pelo usu�rio

Ultima modifica��o: 10/06/2015
*/

#include <iostream>
#include <string>
#include "texto.h"
#include "dicionario.h"

using namespace std;

//ADICIONAR NO DICIONARIO NAO � MAIS ERRO

class Corretor {
	private:
		Texto *texto;
		Dicionario *dicionario;
		string listaErros[10000];
		int quantErros[10000];
		
	public:
		Corretor( string ); // Inicia construtor passando o nome do arquivo texto especificado
		~Corretor();
		void corrigirErro(string); // Corrigir palavra errada
		void selecionarSemelhante(string); // Seleciona palavras semelhantes
		void adicionarDicionario(string); // Adiciona palavra ao dicionario
		void verificaErro(); // Percorre o texto verificando se a palavra esta errada
};
