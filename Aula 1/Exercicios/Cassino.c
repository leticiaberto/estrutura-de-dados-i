#include <stdio.h>
#include <time.h>
#include "vetor.h"

int main()
{
	vet x;
	int cont1, cont2, cont3, cont4, cont5, cont6, n, i,r;

	scanf("%d", &n);//Recebe o numero de vezes
	inicia(&x);

	srand(time(NULL));//Semente da aleatoriedade

	for (i = 0; i < n; i++)
	{
		r = rand() %6 + 1;//Para ir de 1 a 6
		insere (&x, r);
	}

	cont1 = verRepeticao (x, 1);
	cont2 = verRepeticao (x, 2);
	cont3 = verRepeticao (x, 3);
	cont4 = verRepeticao (x, 4);
	cont5 = verRepeticao (x, 5);
	cont6 = verRepeticao (x, 6);

	printf("Quantidade de numeros 1: %d\n", cont1);
	printf("Quantidade de numeros 2: %d\n", cont2);
	printf("Quantidade de numeros 3: %d\n", cont3);
	printf("Quantidade de numeros 4: %d\n", cont4);
	printf("Quantidade de numeros 5: %d\n", cont5);
	printf("Quantidade de numeros 6: %d\n", cont6);

	return 0;
}
