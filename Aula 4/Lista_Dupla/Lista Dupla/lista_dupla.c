/*
 * Implementacao de lista sem n� cabe�a.
 */

#include"lista_dupla.h"
#include<stdio.h>
#include<stdlib.h>

/* Inicializa uma lista */
void inicLista(Lista *p_l){
	*p_l = NULL;
}

/* Verifica se a lista est� vazia ou nao */
int listaVazia(Lista *p_l){
	if (*p_l == NULL)
		return 1;
	return 0;
}

/* Insere um elemento no inicio da lista */
void insereInicio(Lista *p_l, elem_t e){
	No_lista *novo;
	novo = malloc (sizeof(No_lista));
	novo->info = e;
	novo->prox = *p_l;
	novo->ant = NULL;
	*p_l = novo;
}

/* Insere um elemento no final da lista */
void insereFim(Lista *p_l, elem_t e){
	No_lista *novo;
	Lista aux;
	
	novo = malloc (sizeof(No_lista));
	novo->info = e;
	novo->prox = NULL;
	
	aux = *p_l;
	if (aux == NULL)
		*p_l = novo;
	else {
		while(aux->prox != NULL)
			aux = aux->prox;
		aux->prox = novo;
		novo->ant = aux;
	}
}

/* Insere um elemento na lista de maneira ordenada.
   Caso a lista nao esteja ordenada, ordena antes da insercao */
void insereOrdenado(Lista *p_l, elem_t e){
	No_lista *novo;
	Lista aux;
	novo = malloc (sizeof(No_lista));
	novo->info = e;

	if (!ordenada(p_l))
		ordena(p_l);
	aux = *p_l;
	if (aux == NULL){
		*p_l = novo;
		novo->prox = NULL;
		novo->ant = NULL;
	}
	else {
		while(aux->prox != NULL && aux->prox->info < e ){
			aux = aux->prox;
		}
		if (aux->info > e){
			novo->prox = aux;
			novo->ant = aux->ant;
			aux->ant = novo;
			if(novo->ant != NULL) novo->ant->prox = novo;
			*p_l = novo;
		}	
		else {
			novo->prox = aux->prox;
			aux->prox = novo;
			novo->ant = aux;
		}
	}
}

/* Verifica se a lista esta ordenada */
int ordenada(Lista *p_l){
	int ord = 1;
	Lista aux;
	aux = *p_l;
	if (aux == NULL || aux->prox == NULL)
		return 1;
	while(aux->prox != NULL && ord) {
		if (aux->info > aux->prox->info)
			ord = 0;
		aux = aux->prox;
		
	}
	return ord;
}

/* Ordena a lista */
void ordena(Lista *p_l){

	No_lista *aux1, *aux2;
	int temp;

	aux1 = *p_l;
	do {
		aux2 = aux1;
		do {
			aux2 = aux2->prox;
			if(aux1->info > aux2->info) {
				temp = aux1->info;
				aux1->info = aux2->info;
				aux2->info = temp;
			}
		} while(aux2->prox != NULL);
		aux1 = aux1->prox;
	} while(aux1->prox != NULL);

}

/* Remove o elemento que esta no inicio da lista.
   Retorna 0 caso a lista esteja vazia */
int removeInicio(Lista *p_l, elem_t *p_e){
	No_lista *aux;
	aux = *p_l;
	if (aux == NULL)
		return 0;
	*p_l = aux->prox;
	*p_e = aux->info;
	free (aux);
	return 1;
}

/* Remove o elemento que esta no final da lista.
   Retorna 0 caso a lista esteja vazia */
int removeFim(Lista *p_l, elem_t *p_e){
	No_lista *aux;
	aux = *p_l;
	// lista vazia
	if (aux == NULL)
		return 0;
		
	//	um unico elemento
	if (aux->prox == NULL) {
		*p_e = aux->info;
		*p_l = NULL;
		free(aux);
		return 1;
	}
		
	// mais que um elemento
	while(aux->prox != NULL) {
		aux = aux->prox;
	}
	aux->ant->prox = NULL;
	*p_e = aux->info;
	free(aux);
	
	return 1;
}

/* Remove o numero de valor e.
   Retorna 0 caso este numero n�o tenha sido encontrado */
int removeValor(Lista *p_l, elem_t e){
	No_lista *aux;
	aux = *p_l;
	if (aux == NULL)
		return 0;

	//	um unico elemento e eh o elemento a ser removido
	if (aux->prox == NULL && aux->info == e) {
		*p_l = NULL;
		free(aux);
		return 1;
	}


	while(aux->prox != NULL && aux->info != e){
		aux = aux->prox;
	}
	if (aux->info == e){
		if( aux->prox == NULL ) {
			aux->ant->prox = aux->prox;
		} else if( aux->ant == NULL ) {
			aux->prox->ant = aux->ant;
			*p_l = aux->prox;
		} else {
			aux->ant->prox = aux->prox;
			aux->prox->ant = aux->ant;
		}
		free (aux);
		return 1;
	}
	return 0;
}

/* Inverte os elementos de uma lista */
void inverte(Lista *p_l){
	No_lista *aux1, *aux2, *aux3;
	aux1 = *p_l;
	if (aux1 == NULL) // lista vazia
		return;
	aux2 = aux1->prox;
	if (aux2 == NULL) // lista com um unico elemento
		return;
		
	aux1->prox = NULL;
	aux3 = aux2->prox;
	while(aux3 != NULL) {
		aux2->prox = aux1;
		aux1->ant = aux2;
		aux1 = aux2;
		aux2 = aux3;
		aux3 = aux3->prox;
	}
	aux2->prox = aux1;
	aux2->ant = NULL;
	aux1->ant = aux2;
	*p_l = aux2;

}

/* Remove todos os numeros da lista */
void libera(Lista *p_l){
	No_lista *aux;
	aux = *p_l;
	while (aux != NULL) {
		*p_l = aux->prox;
		free (aux);
		aux = *p_l;
	}
}

/* Exibe o conteudo da lista */
void exibe(Lista *p_l){
	Lista aux;
	aux = *p_l;
	while(aux != NULL){
		printf("%d ", aux->info);
		aux = aux->prox;
	}
}

