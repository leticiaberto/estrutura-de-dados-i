/*
 * Implementacao de Pilha com n� cabe�a.
 */

#include"pilha.h"
#include<stdio.h>
#include<stdlib.h>

/* Inicializa uma Pilha */
void inicPilha(Pilha *p_l){
	p_l->prox = NULL;
}

/* Verifica se a Pilha est� vazia ou nao */
int PilhaVazia(Pilha *p_l){
	if (p_l->prox == NULL)
		return 1;
	return 0;
}

/* Insere um elemento na Pilha */
void push(Pilha *p_l, elem_t e){
	No_Pilha *novo;
	novo = malloc (sizeof(No_Pilha));
	novo->info = e;
	novo->prox = p_l->prox;
	p_l->prox = novo;	
}


/* Remove um elemento da Pilha.
   Retorna 0 caso a Pilha esteja vazia */
elem_t pop(Pilha *p_l){
	No_Pilha *aux;
	elem_t e = '\0';
	aux = p_l->prox;
	if (aux == NULL)
		return e;
	p_l->prox = aux->prox; // p_l->prox->prox
	e = aux->info;
	free (aux);
	return e;
}

/* Retorna o elemento do topo da pilha */
elem_t elemTopo(Pilha *p_l){

	if (p_l->prox == NULL)
		return '\0';
	return p_l->prox->info;
}



/* Remove todos os numeros da Pilha */
void libera(Pilha *p_l){
	No_Pilha *aux;
	aux = p_l->prox;
	while (aux != NULL) {
		p_l = aux->prox;
		free (aux);
		aux = p_l;
	}
	p_l->prox = NULL;
}

/* Exibe o conteudo da Pilha */
void exibe(Pilha *p_l){
	while(p_l->prox != NULL){
		printf("%d ", p_l->prox->info);
		p_l = p_l->prox;
	}
}

