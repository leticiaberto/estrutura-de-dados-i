/*
palavra.h
Este m�dulo desenvolve a classe palavra, utilizado para armazenar cada palavra
do texto e tamb�m fornecer op��es para uma poss�vel corre��o da palavra se desejada.

Dados de entrada: Palavra do arquivo de texto
Dados de saida: Palavras para corre��o

Ultima modifica��o: 10/06/2015
*/

#include <iostream>
#include <string>

#ifndef PALAVRA_H_
#define PALAVRA_H_

using namespace std;

class Palavra {
	private:
		string palavra;
	public:
		Palavra();
		string getPalavra(); // Retorna string da palavra
		void setPalavra(string); // Atualiza palavra
		bool operator==(Palavra &);
		bool verificarSemelhante(string); // Retorna 1 caso seja semelhante, ou seja, caso tenha as duas primeiras letras iguais. 0 caso contrario
};

#endif

