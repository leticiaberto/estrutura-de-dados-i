/*
 * Implementacao de lista sem n� cabe�a.
 */

#include"lista_dinamica.h"
#include<stdio.h>
#include<stdlib.h>

/* Inicializa uma lista */
void inicLista(Lista *p_l){
	*p_l = NULL;
}

/* Verifica se a lista est� vazia ou nao */
int listaVazia(Lista *p_l){
	if (*p_l == NULL)
		return 1;
	return 0;
}

/* Insere um elemento no inicio da lista */
void insereInicio(Lista *p_l, elem_t e){
	No_lista *novo;
	novo = malloc (sizeof(No_lista));
	novo->info = e;
	if(listaVazia(p_l))
	{
		novo->prox = NULL;
		novo->ant = NULL;
		*p_l = novo;
	}	
	else
	{
		novo->ant = NULL;
		novo->prox = *p_l;
		(*p_l)->ant = novo;
		*p_l = novo;
	}
		
}

/* Insere um elemento no final da lista */
void insereFim(Lista *p_l, elem_t e){
	No_lista *novo;
	Lista aux;
	
	novo = malloc (sizeof(No_lista));
	novo->info = e;
	novo->prox = NULL;
	
	aux = *p_l;
	if (aux == NULL)//lista vazia
	{
		novo->ant = NULL;
		*p_l = novo;
	}
	else {
		while(aux->prox != NULL)
			aux = aux->prox;
		aux->prox = novo;	
		novo->ant = aux;
	}
}

/* Insere um elemento na lista de maneira ordenada.
   Caso a lista nao esteja ordenada, ordena antes da insercao */
void insereOrdenado(Lista *p_l, elem_t e){
	No_lista *novo;
	Lista aux;
	novo = malloc (sizeof(No_lista));
	novo->info = e;

	if (!ordenada(p_l))
		ordena(p_l);
	aux = *p_l;
	if (aux == NULL){
		*p_l = novo;
		novo->prox = NULL;
	}
	else {
		while(aux->prox != NULL && aux->prox->info < e ){
			aux = aux->prox;
		}	
		if (aux->info > e){
			novo->prox = aux;
			*p_l = novo;
		}	
		else {
			novo->prox = aux->prox;
			aux->prox = novo;
		}
	}
}

/* Verifica se a lista esta ordenada */
int ordenada(Lista *p_l){
	int ord = 1;
	Lista aux;
	aux = *p_l;
	if (aux == NULL || aux->prox == NULL)
		return 1;
	while(aux->prox != NULL && ord) {
		if (aux->info > aux->prox->info)
			ord = 0;
		aux = aux->prox;
		
	}
	return ord;
}

/* Ordena a lista */
void ordena(Lista *p_l){

}

/* Remove o elemento que esta no inicio da lista.
   Retorna 0 caso a lista esteja vazia */
int removeInicio(Lista *p_l, elem_t *p_e){
	No_lista *aux;
	aux = *p_l;
	if (aux == NULL)//lista vazia
		return 0;
	if (aux->prox == NULL)//tem s� um elemento
	{
		*p_l = NULL;
		*p_e = aux->info;
		free(aux);
	}
	else
	{	
		*p_l = aux->prox;
		(*p_l)->ant = NULL;
		*p_e = aux->info;
		free (aux);
	}
	return 1;
}

/* Remove o elemento que esta no final da lista.
   Retorna 0 caso a lista esteja vazia */
int removeFim(Lista *p_l, elem_t *p_e){
	No_lista *aux, *ant;
	aux = *p_l;
	// lista vazia
	if (aux == NULL)
		return 0;
		
	//	um unico elemento
	if (aux->prox == NULL) {
		*p_e = aux->info;
		*p_l = NULL;
		free(aux);
		return 1;
	}
		
	// mais que um elemento
	while(aux->prox != NULL) {
		//ant = aux;
		aux = aux->prox;
	}
	aux->ant->prox = NULL;
	*p_e = aux->info;
	free(aux);
	
	return 1;
}

/* Remove o numero de valor e.
   Retorna 0 caso este numero n�o tenha sido encontrado */
int removeValor(Lista *p_l, elem_t e){
	No_lista *aux, *ant;
	aux = *p_l;
	if (aux == NULL)
		return 0;

	//	um unico elemento e eh o elemento a ser removido
	if (aux->prox == NULL && aux->info == e) {
		*p_l = NULL;
		free(aux);
		return 1;
	}


	while(aux->prox != NULL && aux->info != e){
		ant = aux;
		aux = aux->prox;
	}
	if (aux->info == e){
		ant->prox = aux->prox;
		free (aux);
		return 1;
	}
	return 0;
}

/* Inverte os elementos de uma lista */
void inverte(Lista *p_l){
	No_lista *aux, *aux2, *aux3;
	aux = *p_l;
	if (aux == NULL) // lista vazia
		return;
	
	if (aux->prox == NULL) // lista com um unico elemento
		return;
	aux2 = *p_l;
	while(aux != NULL)
	{
		aux2 = aux->ant;
		aux2->ant = aux->prox;
		aux = aux->ant;
	}
	*p_l = aux;
}

/* Remove todos os numeros da lista */
void libera(Lista *p_l){
	No_lista *aux;
	aux = *p_l;
	while (aux != NULL) {
		*p_l = aux->prox;
		free (aux);
		aux = *p_l;
	}
}

/* Exibe o conteudo da lista */
void exibe(Lista *p_l){
	Lista aux;
	aux = *p_l;
	while(aux != NULL){
		printf("%d ", aux->info);
		aux = aux->prox;
	}
	printf("\n");
}

