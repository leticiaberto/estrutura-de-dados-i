/*
 * Implementacao de listas.
 */

/*Leticia Berto 
RA 587354*/

#include"lista_estatica.h"
#include<stdio.h>

/* Inicializa uma lista */
void inicLista(Lista *p_l)
{
	p_l->tam = 0;
}

/* Verifica se a lista est� vazia ou nao */
int listaVazia(Lista *p_l)
{
	if (p_l->tam == 0)
		return 1;
	return 0;
}

/* Insere um elemento no inicio da lista */
void insereInicio(Lista *p_l, elem_t e)
{
	int i;
	if (p_l->tam == 0)//primeiro elemento a ser inserido
	{
		p_l->item[p_l->tam] = e;
		//printf("valor: %d",p_l->item[p_l->tam]);
		p_l->tam++;
	}
	else
	{	
		if (p_l->tam == MAX)
		{
			printf("Lista cheia!\n");
			return;
		}
		//if (p_l->tam < MAX && p_l->tam != 0)//N�o esta cheia e possui ao menos um elemento
		else
		{
			p_l->tam++;
			for (i= p_l->tam - 1; i > 0; i--)
			{
				p_l->item[i] = p_l->item[i-1];//passa os valores da esquerda para a direita
				
			}
			p_l->item[i] = e;//coloca o novo valor no inicio da lista
		}
	}
}

/* Insere um elemento no final da lista */
void insereFim(Lista *p_l, elem_t e)
{
	if (p_l->tam == MAX)
		printf("Lista cheia!\n");
	else
	{
		p_l->tam++;
		p_l->item[p_l->tam-1] = e;
	}
}

/* Insere um elemento na lista de maneira ordenada.
   Retorna 0 caso o elemento ja exista na lista. 
   Assume que a lista esta ordenada */
int insereOrdenado(Lista *p_l, elem_t e)
{
	int i,j, flag = 1;
	if (p_l->tam == MAX)
		printf("Lista cheia!\n");
	else
	{
		for(i = 0; i < p_l->tam; i++)
			if (p_l->item[i] == e)
				return 0;
		//n�o tem elemento igual
		for(i = 0; i < p_l->tam && flag; i++)
			if (e < p_l->item[i])
			{
				p_l->tam++;
				for(j = p_l->tam-1; j > i; j--)
				{
					p_l->item[j] = p_l->item[j-1];
									
				}		
				p_l->item[i] = e;
				flag = 0;//j� inseriu ordenado
			}
			if (flag == 1)
			{
				p_l->tam++;
				p_l->item[p_l->tam-1] = e;
			}
	}
}

/* Verifica se a lista esta ordenada */
int ordenada(Lista *p_l)
{
	int ord = 1, i=0;
	while(i < p_l->tam - 1 && ord == 1)
	{
		if (p_l->item[i] > p_l->item[i+1])
			ord = 0;
		i++;
	}
	return ord;
}

/* Ordena a lista */
void ordena(Lista *p_l)
{
	int i, aux,j;
	
	for (i = 0; i < p_l->tam; i++)
	{
		for (j = i+1; j < p_l->tam; j++)
		{
			if (p_l->item[i] > p_l->item[j])
			{
				aux = p_l->item[i];
				//printf("aux: %d\n",aux);
				p_l->item[i] = p_l->item[j];
				p_l->item[j] = aux;
			}
		}
	}
}

/* Remove o elemento que esta no inicio da lista.
   Retorna 0 caso a lista esteja vazia */
int removeInicio(Lista *p_l, elem_t *p_e)
{
	int i;
	if (p_l->tam == 0)
		return 0;
	
		*p_e = p_l->item[0];
		
		for (i= 0; i < p_l->tam; i++)
		{
			p_l->item[i] = p_l->item[i+1];//passa os valores da esquerda para a direita
			
		}
		p_l->tam--;
		return(*p_e);
	
}

/* Remove o elemento que esta no final da lista.
   Retorna 0 caso a lista esteja vazia */
int removeFim(Lista *p_l, elem_t *p_e)
{
	if (p_l->tam == 0)
		return 0;
	p_l->tam--;
	*p_e = p_l->item[p_l->tam];
	return(*p_e);
}

/* Remove o numero de valor e.
   Retorna 0 caso este numero n�o tenha sido encontrado */
int removeValor(Lista *p_l, elem_t e)
{
	int i = 0, j,
		flag = 0;//0 enquanto nao acha o valor, 1 qnd achar
	while(i < p_l->tam && flag == 0)
	{
		if (p_l->item[i] == e)
		{
			p_l->tam--;
			for(j = i; j < p_l->tam; j++)
			{
				p_l->item[j] = p_l->item[j+1];
			}
			flag = 1;
		}
		i++;
	}
	
	if (flag == 0)
		return 0;
	return 1;
}

/* Inverte os elementos de uma lista */
void inverte(Lista *p_l)
{
	Lista vet;
	int j,i,aux;
	for (j = p_l->tam-1, i = 0;i< (p_l->tam)/2 ; j--, i++)//troca o primeiro pelo ultimo e assim por diante. faz tam/2 pq "trocas" vao ser a metade de tamanho total
	{
		aux = p_l->item[j];
		p_l->item[j] = p_l->item[i];
		p_l->item[i] = aux;
	}
}

/* Remove todos os numeros da lista */
void libera(Lista *p_l)
{
	p_l->tam = 0;
}

/* Exibe o conteudo da lista */
void exibe(Lista *p_l)
{
	int i = 0;
	while(i < p_l->tam)
	{
		printf("%d ",p_l->item[i]);
		i++;
	}
}


