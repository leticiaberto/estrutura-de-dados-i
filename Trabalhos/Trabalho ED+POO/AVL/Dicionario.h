#include <iostream>
#include <string>
using namespace std; 

class Dicionario {
	public:
		Dicionario();
		~Dicionario();
		void Insere(string n);
		void inOrder();
		void desaloca(); 
		void busca(string n);
		void fornecerSemelhantes(string n);
		void carregarDic();
	private:
		class No{
			public:
				No *esq, *dir; //ponteiros para filhos
				string info; //conteudo
				int h; //altura
				int b; //balanceamento
		};
		void emOrdem(No *x);
		void balanceia(No *&x);
		void atualiza(No *&x);
		void LL(No *&x);
		void RR(No *&x);
		void LR(No *&x);
		void RL(No *&x);
		void desaloca(No *x);
		void insere(No *&x, string el);
		int busca(No *x, string el);
		void fornecerSemelhantes(No *x, string el);
		No *T;
};