#include "fila.h"

void inicFila(Fila *p_l)
{
	p_l->head = 0;
	p_l->tail = -1;
	p_l->qntd = 0;
}

//retorna 0 se nao estiver vazia e 1 caso contrario
int FilaVazia(Fila *p_l)
{
	if (p_l->qntd == 0)
		return 1;
	else
		return 0;
}

void insereFila(Fila *p_l, elem_t e)
{
	if (!FilaCheia(p_l))
	{
		p_l->tail = (p_l->tail + 1) % MAX;
		p_l->info[p_l->tail] = e;
		p_l->qntd++;
	}
}
//retorna 0 se nao estiver cheia e 1 se estiver cheia
int FilaCheia(Fila *p_l)
{
	if (p_l->qntd == MAX)
		return 1;
	else
		return 0;
}

elem_t removeFila(Fila *p_l)
{
	elem_t aux;
	aux = p_l->info[p_l->head];
	
	if(!FilaVazia(p_l))
	{
		p_l->qntd--;
		p_l->head = (p_l->head + 1) % MAX;
		return(aux);
	}
	return 0;
}

void libera(Fila *p_l)
{
	inicFila(p_l);
}

void FuraFila(Fila* p_l, elem_t e)
{
	if(!FilaCheia(p_l))
	{
		p_l->head = (p_l->head - 1) % MAX;
		p_l->info[p_l->head] = e;
		p_l->qntd++;
	}
}
