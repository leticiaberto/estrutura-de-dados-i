#include <stdio.h>
#include "pilha.h"

int main()
{
	char c;
	Pilha p;
	inicPilha(&p);
	
	scanf("%c",&c);
	
	while(c != '\n')
	{
		switch (c)
		{
			case '(':
			case '[':
					push(&p,c);
					break;
			case ']':
					if(pop(&p) != '[')
					{
						printf("Nao balanceada\n");
						return;
					}
					break;
			case ')':
					if(pop(&p) != '(')
					{
						printf("Nao balanceada\n");
						return;
					}
					break;
		}
		scanf("%c", &c);
		
	}
	if(!PilhaVazia(&p))
		printf("Nao balanceada\n");
	else
		printf("Balanceada\n");
	return 0;
}
