#include <iostream>
#include <string>
#include "AVL.h"

using namespace std;
using std::ifstream;

//-----------------------PUBLIC--------------------------
//construtor
AVL::AVL(){
	T = NULL;
}

//destrutor
AVL::~AVL(){
	desaloca(T);
	T = NULL;
}

 //inserção de dados na arvore avl
void AVL::Insere(string n){
	insere(T, n);
}

//impressão em ordem da arvore avl
void AVL::inOrder(){
	emOrdem(T);
}

void AVL::desaloca(){
	desaloca(T);
}

//busca palavras para verificar existencia da palavra
int AVL::busca(string n){
	if(busca(T, n))
		return 1;
	else
		return 0;
}

//-----------------------PRIVATE--------------------------

//impressão em ordem alfabetica da arvore
void AVL::emOrdem(No *x){
	if (x != NULL){
    	emOrdem(x->esq);  
    	cout << x->info << endl;  
		emOrdem	(x->dir);  
	}
}

//verifica o balanceamento de cada nó da arvore
void AVL::balanceia(No *&x){
	if(x->b == 2){
            if(x->esq->b == -1)
                LR(x->esq);
            else
            	RR(x);
	}
	else if(x->b == -2){
            if(x->dir->b == 1)
				RL(x->dir);
			else
            	LL(x);
	}
}

//atualiza o peso de cada nó da arvore
void AVL::atualiza(No *&x){
	int hesq, hdir;
	if(x->esq)
		hesq=x->esq->h;
	else
		hesq=0;
	if(x->dir)
		hdir=x->dir->h;
	else
		hdir=0;
	x->b = hesq-hdir;

}

//rotação LL para o caso de desbalanceamento da arvore
void AVL::LL(No *&x){
	No *aux = x;  
	x = x->dir;  
    aux->dir = x->esq;  
    x->esq = aux;  
    atualiza(aux);  
    atualiza(x);
}

//rotação RR para o caso de desbalanceamento da arvore
void AVL::RR(No *&x){
	No *aux = x;  
    x = x->esq;  
    aux->esq = x->dir;  
	x->dir = aux;  
    atualiza(aux);  
    atualiza(x);  
}

//rotação LR para o caso de desbalanceamento da arvore
void AVL::LR(No *&x) {  //lembrar que b eh o fator de balanceamento
  No *c = x;
  No *a = c->esq;
  No *b = a->dir;
  c->esq = b->dir;
  a->dir = b->esq;
  b->esq = a;
  b->dir = c;
  atualiza(a);
  atualiza(c);
  atualiza(b);
  b->b = 0;
  x = b;
}

//rotação RL para o caso de desbalanceamento da arvore
void AVL::RL(No *&x) {
  No *a = x;
  No *c = a->dir;
  No *b = c->esq;
  c->esq = b->dir;
  a->dir = b->esq;
  b->esq = a;
  b->dir = c;
  atualiza(a);  //a e c sao filhos de b, por isso nessa ordem
  atualiza(c);
  atualiza(b);
  b->b = 0;
  x = b;
}

// Desaloca arvore
void AVL::desaloca(No *x){
	if (x!=NULL){
		desaloca(x->esq);
		desaloca(x->dir);
		delete x;
	}		
}

//inserçao de dados na arvore
void AVL::insere(No *&x, string el){
	if (!x) {  
    	x = new No();
        x->info=el;
    }else{
		if( x->info == el) {
			return;
		}
    	if (x->info > el) 
			insere(x->esq, el);  
        else 
		insere(x->dir, el);  
        atualiza(x);  
        balanceia(x);  
   }  
}

//busca uma palavra na arvore
int AVL::busca(No *x, string el){
	if(x == NULL)
		return 0;
	if(x->info== el)
		return 1;
	else{
		return busca(x->esq, el) + busca(x->dir, el);
	}
}
