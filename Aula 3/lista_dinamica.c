/*
 * Implementacao de lista circular
 */

#include"lista_dinamica.h"
#include<stdio.h>
#include<stdlib.h>

/* Inicializa uma lista */
void inicLista(Lista *p_l){
	p_l->prox = p_l;
}

/* Verifica se a lista est� vazia ou nao */
int listaVazia(Lista *p_l){
	if (p_l->prox == p_l)
		return 1;
	return 0;
}

/* Insere um elemento no inicio da lista */
void insereInicio(Lista *p_l, elem_t e){
	No_lista *novo;
	Lista *aux;
	aux = p_l;
	novo = malloc (sizeof(No_lista));
	novo->info = e;
	novo->prox = aux->prox;
	aux->prox = novo;	
}

/* Insere um elemento no final da lista */
void insereFim(Lista *p_l, elem_t e){
	No_lista *novo,*cab=p_l;
	novo = malloc (sizeof(No_lista));
	novo->info = e;
		
	if(p_l->prox == p_l)//lista vazia
	{
		novo->prox = p_l->prox;
		p_l->prox = novo;
	}	
	else
	{
		No_lista *aux;
		aux = p_l->prox;
		while(aux->prox != cab)
			aux = aux->prox;
		novo->prox = aux->prox;
		aux->prox = novo;
	}
}

void soma(Lista *p_l, Lista *p_l2, Lista *p_l3)
{
	int sob = 0, res = 0;
	No_lista *aux, *aux2, *aux3;
	No_lista *cab = p_l, *cab2 = p_l2, *cab3 = p_l3;
	//printf("Entrou\n");
	aux = p_l->prox;
	aux2 = p_l2->prox;
	while(aux != cab && aux2 != cab2)
	{
		//soma e ve se tem quatro digitos, ai manda o vai um na fun��o
		//aux->info + aux2->info
		res = (aux->info + aux2->info + sob) % 10000;//sob � o vai um da soma
		sob = (aux->info + aux2->info) / 10000;
		
		insereFim(&*p_l3,res);
		
		aux = aux->prox;
		aux2 = aux2->prox;
	}
	if(sob != 0)//deu mais q 4 digitos
		insereFim(&*p_l3,sob);
	if(aux == cab && aux2 != cab2)//a lista dois ainda n�o acabou
	{
		while(aux2 != cab2)
		{	insereFim(&*p_l3,aux2->info);
			aux2 = aux2->prox;
		}
	}
	
	if(aux != cab && aux2 == cab2)//a lista um ainda n�o acabou
	{
		while(aux != cab)
		{	insereFim(&*p_l3,aux->info);
			aux = aux->prox;
		}
	}
}

/* Remove todos os numeros da lista */
void libera(Lista *p_l){
	No_lista *aux;
	aux = p_l->prox;
	while (aux != NULL) {
		if (aux->prox == p_l)//chegou no ultimo elemento da lista(que aponta de volta para o inicio)
		{
			free(aux);
			p_l->prox = p_l;
			return;
		}
		else
		{
			p_l->prox = aux->prox;
			free (aux);
			aux = p_l->prox;}
		}
	
}

/* Exibe o conteudo da lista */
void exibe(Lista *p_l){
	No_lista *cab;
	cab = p_l;
	while(p_l->prox != cab){
		printf("%d ", p_l->prox->info);
		p_l = p_l->prox;
	}
	/*para conferir se o ultimo aponta para o inicio
	p_l = p_l->prox;
	printf("%d ", p_l->prox->info);
	*/
}

