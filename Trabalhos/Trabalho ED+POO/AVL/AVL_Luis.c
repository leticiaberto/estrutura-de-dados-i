/*
 *  Árvore AVL (Adelson-Velskii e Landis)
 *
 *  Renomeie o arquivo incluindo seu nome no final (trab3-avl-Tiemi.c)
 *  Coloque seu nome aqui (este trabalho é individual)
 *
 * Você não deve alterar as funções já escritas, porém poderá incluir funções auxiliares.
 */

#include <stdlib.h>
#include <stdio.h>

typedef struct no {
  int info;
  int bal;   /* hdir - hesq */
  struct no *esq, *dir;
} No;

int altura(No* t) {
  if (t == NULL)
    return 0;
  int hesq = altura(t->esq);
  int hdir = altura(t->dir);
  return hesq > hdir ? hesq + 1 : hdir + 1;
}

No* cria(int chave, No* esq, No* dir) {
  No* n = (No*) malloc (sizeof(No));
  n->info = chave;
  n->bal = altura(dir) - altura(esq);
  n->esq = esq;
  n->dir = dir;
  return n;
}

int verifica_AVL(No* t) {
    if (t==NULL)
        return 1;
    return altura(t->dir) - altura(t->esq);
}

void preorder(No* t){
	if (t != NULL){
		printf("%d ", t->info);
		preorder(t->esq);
		preorder(t->dir);
	}
}

void inorder(No* t){
	if (t != NULL){
		inorder(t->esq);
		printf("%d ", t->bal);
		inorder(t->dir);
	}
}

void postorder(No* t){
	if (t != NULL){
		postorder(t->esq);
		postorder(t->dir);
		printf("%d ", t->info);
	}
}

void LL(No** r) {
  No* b;
  (*r)->bal=0;
  (*r)->esq->bal=0;
  b=(*r)->esq;
  (*r)->esq=b->dir;
  b->dir=(*r);
  (*r)=b;
}

void RR(No** r) {
  No* a = *r;
  No* b = a->dir;
  (*r)->bal=0;
  (*r)->dir->bal=0;
  a->dir = b->esq;
  b->esq = a;
  *r = b;
}

void LR(No** r) {
  No *c = *r;
  No *a = c->esq;
  No *b = a->dir;
  c->esq = b->dir;
  a->dir = b->esq;
  b->esq = a;
  b->dir = c;
  switch(b->bal) {
  case -1:
    a->bal = 0;
    c->bal = 1;
    break;
  case 0:
    a->bal = 0;
    c->bal = 0;
    break;
  case +1:
    a->bal = -1;
    c->bal = 0;
    break;
  }
  b->bal = 0;
  *r = b;
}

void RL(No** r) {
  No *a = *r;
  No *c = a->dir;
  No *b = c->esq;
  c->esq = b->dir;
  a->dir = b->esq;
  b->esq = a;
  b->dir = c;
  switch(b->bal) {
  case -1:
    a->bal = 0;
    c->bal = 1;
    break;
  case 0:
    a->bal = 0;
    c->bal = 0;
    break;
  case +1:
    a->bal = -1;
    c->bal = 0;
    break;
  }
  b->bal = 0;
  *r = b;
}

/* Retorna 1 se inseriu ou 0 se
   o elemento ẽ repetido. */
int insere(No **t, int chave) {
    if (*t==NULL){
        *t=cria(chave,NULL,NULL);
        return 1;
    } else if(chave==(*t)->info){
        return 0;
    /*Caso o valora ser inserido seja menor que a raiz insere no lado esquerdo*/
    } else if (chave < (*t)->info){
        if(insere(&(*t)->esq, chave))
            (*t)->bal-=-1;
        if(altura((*t)->esq)>1){
            if ((*t)->bal>=-1 && (*t)->esq->bal>=-1){
                printf("LL");
                LL(t);
            }else
                   LR(t);
        }
        return 1;
    /*Caso o valora ser inserido seja maior que a raiz insere no lado direito*/
    }else if (chave > (*t)->info){
        printf("AQUI");
        if(insere(&(*t)->dir, chave))
            (*t)->bal+=1;
        if(altura((*t)->dir)>1){
            if ((*t)->bal>=1 && (*t)->dir->bal>=1){
                printf("RR");
                RR(t);
            }else
                   RL(t);
        }
    }
        return 1;
    }

void libera(No** p){
	if(*p != NULL){
		libera(&(*p)->esq);
		libera(&(*p)->dir);
		free (*p);
		*p = NULL;
	}
}

int main() {
  No *t = NULL;
  int num;

  scanf("%d", &num);
  while(num != -1){
    insere(&t, num);
    inorder(t);
    printf("\n");
    preorder(t);
    printf("\n");
    postorder(t);
    printf("\n");
    //if (!verifica_AVL(t))
    //  printf("insercao com problema\n");
    scanf("%d", &num);
  }

  libera(&t);
  return 0;
}
