#include <stdio.h>
#include "vetor.h"
#include <time.h>

#define QNTD 100

int main()
{
	vet x;
	int i, r;

	inicia(&x);
	
	srand(time(NULL));//Semente da aleatoriedade
	printf("Numeros sorteados:\n");
	for (i = 0; i < QNTD; i++)
	{
		r = rand() %100 + 1;//Para ir de 1 a 10
				
		if (verRepeticao (x, r) == 0)
		{
			insere (&x, r);
			printf("%d ", r);
		}
		else
			i--;//Porque quando tira numero repetido soma a quantidade porem todos os numeros nao foram sorteados, logo decrementa-se a quantidade para poder sortear todos os numbers
		
	}
	printf("\n");
	
	return 0;
}
