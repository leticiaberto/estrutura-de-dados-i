#include <iostream>
#include <string>
#include <fstream>
#include "Dicionario.h"

using namespace std;
using std::ifstream;

//-----------------------PUBLIC--------------------------
//construtor
Dicionario::Dicionario(){
	T = NULL;
}

//destrutor
Dicionario::~Dicionario(){
	desaloca(T);
	T = NULL;
}

void Dicionario::carregarDic() {
	ifstream fileDic( "dic.txt", ios::in );	//ios::in eh para gararntir que nao sera modificado
	if( fileDic.is_open() ) {
		string word;
		while( !fileDic.eof() ) { 
			fileDic >> word;
			Insere(word);
		}
	} else {
		cout << "Arquivo não encontrado" << endl;
	}
}

void Dicionario::Insere(string n){
	insere(T, n);
}

void Dicionario::inOrder(){
	emOrdem(T);
}

void Dicionario::desaloca(){
	desaloca(T);
}

void Dicionario::busca(string n){
	if(busca(T, n))
		cout<<"ACHOU"<<endl;
	else
		cout<<"NAO ACHOU"<<endl;
}

void Dicionario::fornecerSemelhantes(string n) {
	fornecerSemelhantes(T, n);
}

//-----------------------PRIVATE--------------------------

void Dicionario::emOrdem(No *x){
	if (x != NULL){
    	emOrdem(x->esq);  
    	cout << x->info<<" ";  
		emOrdem	(x->dir);  
	}
}

void Dicionario::balanceia(No *&x){
	if(x->b == 2){
            if(x->esq->b == -1)
                LL(x->esq);
            RR(x);
	}
	else if(x->b == -2){
            if(x->dir->b == 1)
		RR(x->dir);
            LL(x);
	}
}

void Dicionario::atualiza(No *&x){
	int hesq, hdir;
	if(x->esq)
		hesq=x->esq->h;
	else
		hesq=0;
	if(x->dir)
		hdir=x->dir->h;
	else
		hdir=0;
	x->b = hesq-hdir;
	

}

void Dicionario::LL(No *&x){
	No *aux = x;  
	x = x->dir;  
    aux->dir = x->esq;  
    x->esq = aux;  
    atualiza(aux);  
    atualiza(x);
}

void Dicionario::RR(No *&x){
	No *aux = x;  
    x = x->esq;  
    aux->esq = x->dir;  
	x->dir = aux;  
    atualiza(aux);  
    atualiza(x);  
}

void Dicionario::LR(No *&x) {  //lembrar que b eh o fator de balanceamento
  No *c = x;
  No *a = c->esq;
  No *b = a->dir;
  c->esq = b->dir;
  a->dir = b->esq;
  b->esq = a;
  b->dir = c;
  atualiza(a);
  atualiza(c);
  atualiza(b);
  b->b = 0;
  x = b;
}

void Dicionario::RL(No *&x) {
  No *a = x;
  No *c = a->dir;
  No *b = c->esq;
  c->esq = b->dir;
  a->dir = b->esq;
  b->esq = a;
  b->dir = c;
  atualiza(a);  //a e c sao filhos de b, por isso nessa ordem
  atualiza(c);
  atualiza(b);
  b->b = 0;
  x = b;
}

void Dicionario::desaloca(No *x){
	if (x!=NULL){
		desaloca(x->esq);
		desaloca(x->dir);
		delete x;
		atualiza(x);
		balanceia(x);
	}		
}

void Dicionario::insere(No *&x, string el){
	if (!x) {  
    	x = new No();  
        x->info = el;  
    }else{
		if(x->info == el)
			return;  
    	if (x->info > el) 
			insere(x->esq, el);  
        else 
		insere(x->dir, el);  
        atualiza(x);  
        balanceia(x);  
   }  
}

int Dicionario::busca(No *x, string el){
	if(x == NULL)
		return 0;
	if(x->info == el)
		return 1;
	else{
		return busca(x->esq, el) + busca(x->dir, el);
	}
}

void Dicionario::fornecerSemelhantes(No *x, string el) {

	int cont = 0;
	//if(strncmp(const char * str1, const char * str2, size_t num));

	if(x == NULL)
		cout<< endl;
	for (int i = 0; i < 2; i++) {
		if (x->info[i] == el[i])
			cont++;
	}
	if(cont == 2)
		cout<< x->info << endl;
	
	if (x->esq != NULL)
		fornecerSemelhantes(x->esq, el);
	if (x->dir != NULL)
	fornecerSemelhantes(x->dir, el);
}