#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INSERIDO "Inserido com sucesso!\n"
#define CODIGO_USADO "O codigo ja esta sendo usado!\n"
#define LISTAS_CHEIAS "Listas Cheias!\n"
#define LISTAS_APAGADAS "Todas as listas foram apagadas!\n"
#define PACIENTE_N_ENCONTRADO "Paciente nao encontrado!\n"
#define PACIENTE_REMOVIDO "%s foi removido!\n"

typedef struct no {
  int info;
  char nome[30];
  struct no *esq, *dir;
} No;

typedef No* Arvore;

void cria_arvore(Arvore *p);
void inorder(Arvore p); 
void preorder(Arvore p); 
void posorder(Arvore p); 
int n_rec_busca(Arvore p, int chave); 
int  insere(Arvore *p, int chave, char *n);
int remove_arv(Arvore * p, int chave);

int main() {
	Arvore a;
	int i, cod, opc, qntd, listar;
	char nome[30];

	cria_arvore(&a);

	scanf("%d", &opc);
    do {
    	switch (opc) {
        	case 1:
				scanf("%d", &qntd);
                for (i = 0; i < qntd; i++)
                {
                   	scanf("%d", &cod);
                	scanf("%*c%[^\n]", nome);
                	if (insere(&a, cod, nome)) {
                		printf(INSERIDO);
                	} else {
                		printf(CODIGO_USADO);
                	}
                }
            	break;
            case 2:
                scanf("%d",&cod);
	            if(!remove_arv(&a, cod)) {
	            	printf(PACIENTE_N_ENCONTRADO);
	            } 
                break;
            case 3:
            	scanf ("%d", &cod);
              	if(!(n_rec_busca(a, cod))) {
              		printf(PACIENTE_N_ENCONTRADO);
              	}
              	break;
            case 4:
            	scanf("%d", &listar);
            	switch(listar) {
            		case 1:
           				inorder(a);
           			break;
           			case 2:
           				preorder(a);
           			break;
           			case 3:
           				posorder(a);
           			break;
            	}
                break;
            case 5:
            	cria_arvore(&a);
            	printf("A arvore de busca foi apagada!\n");
                break;
			case 0:
                return 0;
        }
        scanf("%d", &opc);
        qntd = 0;
    } while (opc != 0);
	  
	  return 0;

}

void cria_arvore(Arvore *p) {
	*p = NULL;
}

void inorder(Arvore p) {
	if (p != NULL) {
		inorder (p->esq);
		printf("%d ", p->info); 
		printf("%s\n", p->nome);   
		inorder (p->dir);    
	}
}

void preorder(Arvore p) {
	if (p != NULL) {
		printf("%d ", p->info); 
		printf("%s\n", p->nome);
		preorder (p->esq);
		preorder (p->dir);    
	}
}

void posorder(Arvore p) {
	if (p != NULL) {
		posorder (p->esq);
		posorder (p->dir);
		printf("%d ", p->info); 
		printf("%s\n", p->nome);    
	}
}

/* Retorna 1 se a chave for encontrada */
// versao nao recursiva
int n_rec_busca(Arvore p, int chave) {

	while (p != NULL && p->info != chave) {
		if (p->info > chave) {
			p = p->esq;
		} else {
			p = p->dir;
		}
	}

	if (p != NULL && chave == p->info) {
		printf("%s\n", p->nome);
		return 1;
	}

	return 0;
}

/* Retorna 0 se a chave for repetida */
int  insere(Arvore *p, int chave, char *n) {
	No *novo;

	if (*p == NULL) {
		novo = malloc(sizeof(No));
		novo->info = chave;
		strcpy(novo->nome, n);
		novo->dir = NULL;
		novo->esq = NULL;
		*p = novo;
		return 1;
	}

	if ((*p)->info > chave) {
		return insere((&(*p)->esq), chave, n);
	} else if ((*p)->info < chave) {
		return insere((&(*p)->dir), chave, n);
	} else {
		return 0;
	}

}


int remove_arv(Arvore * p, int chave) {
    No *rem, *aux;
    No **paux;
 
    while ((*p) != NULL && (*p)->info != chave) {
        if ((*p)->info > chave) {
            p = &(*p)->esq;
        } else {
            p = &(*p)->dir;
        }
    }
    //na saida do while, p estara apontando para o endereco da esquerda
 
    if (*p == NULL) {
        return 0;
    }
    rem = *p;
 
    printf(PACIENTE_REMOVIDO, rem->nome);
 
    //no folha
    if (rem->esq == NULL && rem->dir == NULL) {
        *p = NULL;
        free(rem);
        return 1;
    }
 
    //um unico filho
    else if (rem->esq == NULL && rem->dir != NULL) {
        *p = rem->dir;
        free(rem);
        return 1;
    }   else if (rem->esq != NULL && rem->dir == NULL) {
        *p = rem->esq;
        free(rem);
        return 1;
        }
    
    //dois filhos
         
    paux = &rem->esq;
    while ((*paux)->dir != NULL) {
        paux = &(*paux)->dir;
    }
    aux = *paux;
    *paux = aux->esq;
    aux->esq = rem->esq;
    aux->dir = rem->dir;
    *p = aux;
    free(rem);  
 
    return 1;
 
}
