#include<stdio.h>
#include<string.h>
#include"pilha.h"

// retorna 1 se op1 tem precedencia >= a op2
int verificaPrec(char op1, char op2){
	if (op1 == '^')
		return 1;
	if (op1 == '*' || op1 == '/'){
		if (op2 != '^'){
			return 1;
		}
	}
	else 
		if (op2 == '+' || op2 == '-')
			return 1;
	return 0;

}

int main(){
	Pilha s;
	char aux;
	//char inf[] = "(a-b)/(c+d)*e";
	char inf[] = "A^B*C-D+E/F/(G-H)";
	int i = 0;
		
	inicPilha(&s);
	for(i = 0; i < strlen(inf); i++) {
		//printf("%c", inf[i]);
		switch(inf[i]) {
			case '(': push(&s, inf[i]); break;
			case '*':
			case '+':
			case '/':
			case '-':
			case '^':
				while(!PilhaVazia(&s) && verificaPrec(elemTopo(&s), inf[i]) &&
						elemTopo(&s) != '(')
					printf("%c", pop(&s));
				push(&s, inf[i]);
				break;
			case ')': 
				aux = pop(&s);
				while(aux != '('){
					printf("%c", aux);
					aux = pop(&s);
				}
				break;
			default: printf("%c", inf[i]);
				break;
		}
		
	}
	while(!PilhaVazia(&s))
		printf("%c", pop(&s));
		

	printf("\n");

	return 0;
}
