#include<stdio.h>
#include"vetor.h"

int main(){
	int x, i;
	vet v;
	inicia(&v);
	for (i = 0; i < 20; i++)
		insere(&v, i);
	while(!vazio(v)){
		retira(&v, &x);
		if (x > 10)
			printf("%d ", x);
	}
	return 0;
}
