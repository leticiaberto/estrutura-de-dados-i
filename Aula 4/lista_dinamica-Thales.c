/*
 * Implementacao de listas duplas.
 */

#include"lista_dinamica.h"
#include<stdio.h>
#include<stdlib.h>

/* Inicializa uma lista */
void inicLista(Lista *p_l){
	*p_l = NULL;
}

/* Verifica se a lista est� vazia ou nao */
int listaVazia(Lista *p_l){
	if (*p_l == NULL)
		return 1;
	return 0;
}

/* Insere um elemento no inicio da lista */
void insereInicio(Lista *p_l, elem_t e){
	No_lista *novo;
	novo = malloc (sizeof(No_lista));
	novo->info = e;
	
	if (listaVazia(p_l)){
		novo->prox = *p_l;
		novo->ant = *p_l;
		*p_l = novo;
	}else{
		novo->prox = *p_l;
		novo->ant = (*p_l)->ant;
		(*p_l)->ant = novo;
		*p_l = novo;
	}
}

/* Insere um elemento no final da lista */
void insereFim(Lista *p_l, elem_t e){
	No_lista *novo;
	Lista aux;
	
	aux = *p_l;
	novo = malloc (sizeof(No_lista));
	novo->info = e;
	
	if (listaVazia(p_l)){
		novo->prox = *p_l;
		novo->ant = *p_l;
		*p_l = novo;
	}else{
		while(aux->prox != NULL)
			aux = aux->prox;
		novo->prox = aux->prox;
		novo->ant = aux;
		aux->prox = novo;
	}
}

/* Insere um elemento na lista de maneira ordenada.
   Caso a lista nao esteja ordenada, ordena antes da insercao */
void insereOrdenado(Lista *p_l, elem_t e){
	No_lista *novo;
	Lista aux;

	novo = malloc (sizeof(No_lista));
	novo->info = e;

	if (!ordenada(p_l))
		ordena(p_l);
	aux = *p_l;
	if (listaVazia(p_l)){
		insereInicio(p_l, e);
	}
	else{
		while(aux->prox != NULL && aux->prox->info < e ){
			aux = aux->prox;
		}	
		if (aux->info > e){
			novo->prox = *p_l;
			novo->ant = (*p_l)->ant;
			(*p_l)->ant = novo;
			*p_l = novo;
		}	
		else {
			novo->prox = aux->prox;
			novo->ant = aux;
			aux->prox = novo;
		}
	}
}

/* Verifica se a lista esta ordenada */
int ordenada(Lista *p_l){
	int ord = 1;
	Lista aux;
	aux = *p_l;
	if (aux == NULL || aux->prox == NULL)
		return 1;
	while(aux->prox != NULL && ord) {
		if (aux->info > aux->prox->info)
			ord = 0;
		aux = aux->prox;
		
	}
	return ord;
}

/* Ordena a lista */
void ordena(Lista *p_l){
	Lista j, aux;
	int i, cont;
	cont = 0;
	
	j = *p_l;
	while (j->prox != NULL){
		cont++;
		j = j->prox;
	}
	
	j = *p_l;
	for(i = 0; i<=cont; i++){
		while (j->prox != NULL){
			if (j->info > j->prox->info){
				aux = j->prox;
				aux->ant = j->ant;
				
				j->prox = aux->prox;
				j->ant = aux;
				
				aux->prox = j;
				
				if (j == *p_l){
					*p_l = aux;
				}else{
					aux->ant->prox = aux;
				}
			}else{
				j = j->prox;
			}
		}
		j = *p_l;
	}
}

/* Remove o elemento que esta no inicio da lista.
   Retorna 0 caso a lista esteja vazia */
int removeInicio(Lista *p_l, elem_t *p_e){
	No_lista *aux;
	aux = *p_l;
	if (aux == NULL)
		return 0;
	*p_l = aux->prox;
	aux->prox->ant = NULL;
	*p_e = aux->info;
	free (aux);
	return 1;
}

/* Remove o elemento que esta no final da lista.
   Retorna 0 caso a lista esteja vazia */
int removeFim(Lista *p_l, elem_t *p_e){
	No_lista *aux;
	aux = *p_l;
	// lista vazia
	if (aux == NULL)
		return 0;
		
	//	um unico elemento
	if (aux->prox == NULL) {
		*p_e = aux->info;
		*p_l = NULL;
		free(aux);
		return 1;
	}
		
	// mais que um elemento
	while(aux->prox != NULL)
		aux = aux->prox;
	aux->prox->ant = NULL;
	*p_e = aux->info;
	free(aux);
	
	return 1;
}

/* Remove o numero de valor e.
   Retorna 0 caso este numero n�o tenha sido encontrado */
int removeValor(Lista *p_l, elem_t e){
	No_lista *aux;
	aux = *p_l;
	if (aux == NULL)
		return 0;

	//	um unico elemento e eh o elemento a ser removido
	if (aux->prox == NULL && aux->info == e) {
		*p_l = NULL;
		free(aux);
		return 1;
	}


	while(aux->prox != NULL && aux->info != e){
		aux = aux->prox;
	}
	if (aux->info == e){
		if (aux->ant == NULL){//primeiro elemento da lista
			*p_l = (*p_l)->prox;
			(*p_l)->ant = NULL;
			free(aux);
			return 1;
		}else if (aux->prox == NULL){//ultimo elemento da lista
			aux->ant->prox = NULL;
			free(aux);
			return 1;
		}else{//entre dois outros elementos
			aux->ant->prox = aux->prox;
			aux->prox->ant = aux->ant;
			free (aux);
			return 1;
		}
	}
	
	return 0;
}

/* Inverte os elementos de uma lista */
void inverte(Lista *p_l){
	No_lista *aux;
	
	// lista vazia
	if (*p_l == NULL)
		return;

	// lista com um unico elemento
	if ((*p_l)->prox == NULL)
		return;
		
	while(*p_l != NULL){
		aux = (*p_l)->ant;
		(*p_l)->ant = (*p_l)->prox;
		(*p_l)->prox = aux;
		aux = *p_l;
		*p_l = (*p_l)->ant;
	}
	*p_l = aux;
}

/* Remove todos os numeros da lista */
void libera(Lista *p_l){
	No_lista *aux;
	aux = *p_l;
	while (aux != NULL) {
		*p_l = aux->prox;
		free (aux);
		aux = *p_l;
	}
}

/* Exibe o conteudo da lista */
void exibe(Lista *p_l){
	Lista aux;
	aux = *p_l;
	while(aux != NULL){
		printf("%d ", aux->info);
		aux = aux->prox;
	}
}

